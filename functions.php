<?php
/**
 * PHP Functions
 * @package xlbil
 */

/**
 * Formats sum for money display
 * @param  str $kr    sum
 * @param  bol $sign  should the function return with 000,- ending
 * @param  bil $clean should the function just clean the sum
 * @return str        formated string for display
 */
function format_kroner( $kr, $sign = null, $clean = null ) {
	// Clean up after user
	$kr = preg_replace('/(,|\s|\.|-)/', '', trim($kr));

	// return clean number without spaces or points if clean is true
	if( $clean ) return $kr;

	// Convert to array
	$arr = str_split($kr);
	// reverse array
	$arr = array_reverse($arr);
	
	// Add spaces to sum
	$formated = preg_replace('/(\d{3})(\d{3})?(\d{1,3})?/', "$1 $2 $3", implode('', $arr));
	
	// Convert back to array
	$formated = str_split(trim($formated));
	// Reverse to correct order
	$formated = array_reverse($formated);

	// Return string
	return $sign ? implode('', $formated).",-" : implode('', $formated);
}

/**
 * Get or create imagesize
 * @param  string $img  full path to original image
 * @param  array  $size array with size, ex array(width, height)
 * @return string       Path to image size
 */
function zp_im_retreive( $img, $size ) {
	$filename = zp_im_image_filename( $img, $size);
	if ( ! $filename ) {
		// using the function wrong, ex zp_im_retreive("image-660x377.jpg", array(660,377))
		return $img;
	}

	if ( file_exists( $filename ) ) {
		// file size exists return with path, no need to recreate
		return $filename;
	}

	if ( zp_im_create_format( $img, $size ) ) {
		// new size created
		return $filename;
	}

	// No new size could be created return with original path
	return $img;
}

/**
 * Imagick function for thumbnails.
 * Kjør helst etter at bildet er opplastet.
 * @param  string $img  Path to original image
 * @param  array  $size array(width, height)
 * @return bol          null if image exists
 */
function zp_im_create_format($img , $size) {

	$newfilename = zp_im_image_filename( $img, $size );
	// image already has size in name
	if ( ! $newfilename ) {
		return;
	}

	// imagick object
	$im = new Imagick( realpath( $img ) );

	// ratio
	$ratio = $im->getImageWidth() / $im->getImageHeight();

	// landscape picture
	if ( $ratio > 1 ) {
		// use height as starting scale and crop width
		// file_put_contents( ".dumpfile", var_export( $img . " ratio from width / ratio " . round((120 / $ratio)), true ), FILE_APPEND );
		if ( round(($size[0] / $ratio)) < $size[1]  ) {
			$im->scaleImage( 0, $size[1], FALSE );
			$cX = ($im->getImageWidth() - $size[0]) / 2;
			$im->cropImage( $size[0], $size[1], $cX, 0 );
		}
		// use width as starting scale and crop height
		else {
			$im->scaleImage( $size[0], 0, FALSE );
			$cY = ($im->getImageHeight() - $size[1]) / 2;
			$im->cropImage( $size[0], $size[1], 0, $cY );
		}
	}
	// portrait picture
	else {
		// scale down to width and auto height
		$im->scaleImage( $size[0], 0, FALSE );
		$cY = ($im->getImageHeight() - $size[1]) / 2;
		$im->cropImage( $size[0], $size[1], 0, $cY );
	}
	
	$im->writeImage( $newfilename );

	return true;
}

/**
 * Append size to image name
 * @param  string $file Path to image 
 * @param  array  $size Size in px ex. array(120,70)
 * @return bol          Bol false if image exists of new name if not,
 */
function zp_im_image_filename( $file, $size ) {
	// filedata - keys dirname, basename, extension, filename
	$fd = pathinfo( $file );
	$addsize = implode('x', $size);
	$newfile = sprintf( '%s/%s-%s.%s', $fd['dirname'], $fd['filename'], $addsize, $fd['extension']);

	// dont override image please
	if ( stripos( $fd['filename'], $addsize) !== FALSE ) {
		return;
	}

	// New size filename
	return sprintf( '%s/%s-%s.%s', $fd['dirname'], $fd['filename'], $addsize, $fd['extension'] );
}

?>