<?php
/**
 * Min side
 * @package xlbil
 */
require('header.php');
?>
		<main>
			<section class="main-section">
				<div class="row">
					<h1>Om XL Bil</h1>

					<div class="acordion-panels spacer">

						<div id="about-faq-panel" class="panel">
							<h3><button type="button" class="expandable-control" aria-controls="about-faq" aria-expanded="false">Vanlige spørsmål og svar</button></h3>
							<article id="about-faq" class="expandable-content" aria-hidden="true">
								<ol>
									<li><strong>Må jeg selge til høyeste bud på auksjonen?</strong> Nei, du velger selv om du vil godta høyeste bud eller ikke.</li>
									<li><strong>Hvorfor skal jeg selge bilen på xlbil.no?</strong> Det er flere gode grunner til å selge bilen på xlbil.no. Normalt tar det ca 120 dager å få solgt en bruktbil. Her tar det kun 3 dager! Du slipper 5 års reklamasjonsrett fordi du selger til en profesjonell aktør. Undersøkelse som Rødboka utførte i 2014 dessuten at det kun er 3 % forskjell på å selge til bilhandlere vs privat.</li>
									<li><strong>Kan jeg selge bil med heftelser?</strong> Ja, bilhandlere betaler da ut eventuelle heftelser, og resterende beløp på kjøpesummen overføres din konto.</li>
									<li><strong>Hvorfor er det gratis å bruke xlbil.no?</strong> Det er gratis for private og bilhandlere å bruke xlbil.no. Det er kun når en bil blir solgt at bilhandlere betaler et salær til oss.</li>
									<li><strong>Kan private få tilgang til å by på bilen?</strong> Nei, det er kun registrerte bilhandlere som kan by på biler.</li>
									<li><strong>Hvordan avleverer jeg bilen når den er solgt?</strong> Selger leverer bilen uten kostnader for frakt til bilhandler inntil 150 km. For avstander over dette avtales dette direkte mellom selger og bilhandler.</li>
									<li><strong>Kan jeg legge ut bilen flere ganger på xlbil.no?</strong> Ja, du kan legge ut bilen inntil 3 ganger på auksjonen.</li>
								</ol>
							</article>
						</div><!-- /.panel -->

						<div id="about-contact-panel" class="panel">
							<h3><button type="button" class="expandable-control" aria-controls="about-contact" aria-expanded="false">Kontakt XLBil</button></h3>
							<article id="about-contact" class="expandable-content" aria-hidden="true">
								<p><strong>Har du spørsmål om objektet?</strong><br>
								Dersom det ikke er oppgitt kontaktinfomasjon i annonsen, selges objektet med den infomasjonen som er oppgitt.</p>

								<p><strong>Har du nylig registrert konto?</strong><br>
								Alle nye registreringer godkjennes av XLBil. Du vil motta en e-mail så snart din konto er aktivert. Dette skjer senest neste virkedag, men alle nye kontoer som opprettes hverdager før kl 1400 mottar en e-mail innen kl 1400 samme dag og vil ha mulighet til å delta i auksjonene.</p>

								<p><strong>Jeg hadde høyeste bud, hva nå?</strong><br>
								Selger står fritt om han vil godta høyeste bud eller ikke. Dersom selger godtar høyeste bud får kjøper umiddelbar beskjed per e –post.</p>

								<p><strong>For andre spørsmål kan du nå oss på e-mail:</strong><br>
								E-post: support@xlbil.no<br><br>

								<strong>Postadresse:</strong><br></p>
								<address>
									<strong>XL BIL AS</strong><br>
									Hystadvegen 178<br>
									5416 Stord
								</address>
							</article><!-- /.panel -->
						</div>

						<div id="about-us-panel" class="panel">
							<h3><button type="button" class="expandable-control" aria-controls="about-us" aria-expanded="false">Om XLBil</button></h3>
							<article id="about-us" class="expandable-content" aria-hidden="true">
								<p>Xlbil holder til i Ringevegen 30 på Stord. Xlbil har en meget effektiv og kundevennlig auksjon. Vi har som målsetting å være Norges raskeste auksjon for kjøretøy. Tilbakemeldinger fra private og forhandlere er utelukkede positive.</p>

								<p>Vi ser derfor lyst på fremtiden og vil hele tiden bestrebe oss for å være Norges nummer 1 auksjon.</p>

								<address>
									<strong>Forhandlerpris AS</strong><br>
									Ringevegen 30<br>
									5412 Stord.<br>
									Telefon: 00000000<br>
									E – post: kundeservice@xlbil.no
								</address>
							</article><!-- /.panel -->
						</div>

						<div id="about-terms-panel" class="panel">
							<h3><button type="button" class="expandable-control" aria-controls="about-terms" aria-expanded="false">Vilkår og auksjonsregler</button></h3>
							<article id="about-terms" class="expandable-content" aria-hidden="true">
								<p>Vi tar forbehold om skrivefeil og logiske feil på våre sider.<p>
								<ol>
									<li><strong>Registrering</strong><br>
										For å kunne handle hos XLbil.no må du være registrert. Registreringen er gratis, og uten forpliktelser for deg. Ved å fylle ut registreringsskjemaet bekrefter du å ha oppgitt korrekte opplysninger om deg selv eller bedriften. Dersom opplysningene du har avgitt er uriktige eller endrer seg i fremtiden er du forpliktet til å underrette XLbil.no med korrekte opplysninger ved å endre din profil under Min konto.
										<br><br>
										Ved første gangs registrering for forhandlere vil dine opplysninger vil bli sendt til manuell verifisering og godkjenning før kontoen aktiveres. Dette fordi det kun er bilforhandlere som kan registreres som kjøpere. Du vil motta en e-post så snart kontoen er aktivert. Opplysningene vil bli lagret på en sikker måte, og vil ikke være tilgjengelige for uvedkommende. Du bestemmer selv brukernavn og passord når du registrerer deg.
										<br><br>
										Det er kun personer som har fylt 18 år og har rett til å inngå juridisk bindende avtaler som kan handle hos XLbil.no. Innlagt bud er bindende. Falsk ordreinnleggelse eller budgivning vil bli politianmeldt. Forsøk på svindel, dokumentforfalskning og annen kriminell virksomhet vil bli strafferettslig forfulgt.
										<br><br>
										Det er gratis å registrere seg som kjøper og selger hos XLbil.no
										<br><br>
									</li>
									<li><strong>Personvern og databehandling</strong><br>
										Ved å akseptere vilkårene hos XLbil.no har du gitt tillatelse til å la XLbil.no benytte dine/selskapets personlige opplysninger i henhold til lov om personvern.
										<br><br>
										XLbil.no er ansvarlig for alle personlige opplysninger du oppgir, eller som vi får del i når du legger inn bud eller besøker vår markedsplass. XLbil.no vil under ingen omstendigheter overlate de utleverte personalia, eller andre opplysninger om kunder hos XLbil.no, til en tredjepart. Ved å melde deg inn hos XLbil.no gir du selskapet tillatelse til å bruke din e-post adresse til å sende markedsføring om selskapet og auksjoner. Dette kan du imidlertid reservere deg mot på innmeldingsskjemaet. Du har på forespørsel rett til å få opplyst hvilke opplysninger vi har registrert om deg, og til å kreve dem slettet. Kontakt i så fall <a href="mailto:kundeservice@xlbil.no">kundeservice@xlbil.no</a>
										<br><br>
									</li>
									<li><strong>Auksjonsregler</strong><br><br>
										<ol>
											<li><strong>Bindende bud.</strong><br>
												Alle bud inngitt på auksjonen er juridisk bindende. Dette innebærer at plasseringen av et bud via din konto hos XLbil.no (foretatt enten av deg eller andre som disponerer din konto) ikke kan tilbakekalles fra auksjonen etter at det er avgitt. Er ditt bud vinnende anses en avtale for inngått mellom deg og selger når auksjonen stenger.
												<br><br>
												Det tas imidlertid forbehold om at dersom et bud åpenbart er feil kan dette annulleres.
												<br><br>
											</li>
											<li>
												<strong>Avslå eller godta bud.</strong><br>
												Selger har rett til å avslå ethvert vinnerbud.
												<br><br>
												Når en auksjon er avsluttet og ditt vinnerbud er registrert, vil du få en bekreftelse fra selger innen 24 timer om budet er godtatt eller avslått.
												<br><br>
											</li>
											<li>
												<strong>Stenging av auksjoner</strong><br>
												XLbil.no forbeholder seg retten til å stenge muligheten for salg på gitte tidspunkter.
												<br><br>

												<strong>Stenge ute useriøse brukere</strong><br>
												XLbil.no forbeholder seg retten til å utestenge useriøse brukere, slette auksjoner, og slette bud.
												<br><br>
											</li>
											<li>
												<strong>Salær og betalingsbetingelser bilforhandlere.</strong><br>
												Produkter kjøpt hos XLbil.no betales med faktura. Fakturagebyr tilkommer på hver ordre.<br><br>

												For kjøp av bil inntil kr. 199 999.- tilkommer et salær på kr. 1000.- + mva.<br>
												For kjøp av bil fra kr. 200 000.- tilkommer et salær på kr. 2000.- + mva<br>
												Betalingsfrist er 14 dager.<br><br>

												Dersom en handel av ulike årsaker ikke kan gjennomføres vil salær allikevel måtte betales.<br><br>

											</li>
											<li>
												<strong>Frakt og avhenting</strong><br>
												Dersom ikke annet er avtalt plikter selger å levere kjøretøyet vederlagsfritt på kjøpers adresse inntil 150 km fra selgers adresse.<br><br>
											</li>
											<li>
												<strong>Bud samarbeid</strong><br>
												Det er ikke tillatt med samarbeid som begrenser konkurransen, jfr. konkurranseloven kap. 3 § 10.<br><br>
											</li>
											<li>

												<strong>Bud økning</strong><br>
												Minimum bud økning er kr. 1000.-<br><br>
											</li>
										</ol>
									</li>
									<li>
										<strong>Xlbil.no rolle</strong><br>
										XLbil.no kan ikke bli holdt ansvarlig for oppføringen av eller de kjøretøy som tilbys for salg eller levering av selgeren. XLbil.no har ikke kontroll over, eller ansvar for kvaliteten, sikkerheten, eller lovligheten av kjøretøyet som er gjenstand for slike auksjoner. Selger er derimot forpliktet til å levere et produkt i den stand som er beskrevet i auksjonen. XLbil.no er selv ikke part i noen av auksjonene og tilbyr kun de tekniske mulighetene, og en markedsplass, hvor disse auksjonene kan bli gjennomført.
										<br><br>

										XLbil.no forbeholder seg retten til å stenge auksjoner før de avsluttes, til å forlenge auksjoner, til å avlyse eller tilbakekalle beskrivelser, hvor vi har tvingende juridiske eller tekniske grunner for å gjøre dette (inkludert tekniske problemer opplevd av XLbil.no, en selger eller på internett). I den grad det er praktisk mulig, vil vi gi en rimelig forhåndsadvarsel angående tiltak som nevnt ovenfor som aktes gjennomført av XLbil.no. Det kan også forekomme tilfeller hvor denne nettjenesten blir utilgjengelig, enten på planlagt eller ikke planlagt grunnlag. Du tilkjennegir at XLbil.no ikke er avtalerettslig (eller på noen annen måte) forpliktet eller behøver å tilby eller gjøre tilgjengelig til deg noen av auksjonene, eller noen annen nettjeneste.
										<br><br>

										Ethvert medlem erkjenner at vi har rett til å avlyse enhver auksjon som forekommer oss - basert på informasjon mottatt fra tredjepersoner eller andre medlemmer - å medføre et mislighold av denne avtale. For dette tilfelle vil vi informere både selgeren og under tiden en eller flere bud givende medlemmer om avlysningen av auksjonen. 
										<br><br>
										
										XLbil.no forbeholder seg retten til å suspendere eller stenge kontoen og til å suspendere eller avslutte enhver auksjon tilknyttet en slik konto til et medlem som misligholder eller som for oss synes å vedvarende eller vesentlig misligholde denne avtale, herunder inkludert avgivelse av falsk registreringsinformasjon eller annen misbruk av markedsplassen XLbil.no. En person som får sin konto suspendert eller avsluttet av XLbil.no kan ikke på nytt registrere seg for bruk av markedsplassen XLbil.no uten vårt forhåndssamtykke. Ved misbruk forbeholder XLbil.no seg retten til å oppbevare data tilknyttet den stengte/suspenderte konto, for intern bruk. XLbil.no gjør oppmerksom på at forsøk på ulovlig aktivitet og misbruk av våre tjenester på www.xlbil.no vil kunne bli overlevert til politiet ved straffbare forhold for rettslig oppfølging.
										<br><br>

										XLbil.no gjør oppmerksom på at auksjon på nett av brukte produkter ikke omfattes av angrerettlovene. Det er således ingen angrerett.
										<br><br>
									</li>
									<li>
										<strong>Xlbil.no ansvar</strong><br>
										I den utstrekning dette tillates av norsk lov frafaller du ethvert krav mot XLbil.no, dets representanter og ansatte som måtte oppstå i forbindelse med eller i sammenheng med enhver auksjon, inkludert (men ikke begrenset til) ethvert krav eller forlangende i forbindelse med beskrivelsen av kjøretøyet, bud, ikke gjennomførte eller gjennomførte transaksjoner, eller produkter tilbudt gjennom eller i forbindelse med enhver auksjon. 
										<br><br>

										XLbil.no er ikke ansvarlig for manglende gjennomføring av forpliktelser etter denne avtale, dersom dette skyldes forhold som ligger utenfor Xlbil.no kontroll.
										<br><br>

										Vi er ikke ansvarlige overfor deg eller overfor noen tredjepart for indirekte- eller andre følgeskader eller tap, eller for noe tap av data, profitt, inntekter, utbytte eller omsetning, uavhengig av hvordan skaden eller tapet har oppstått (enten skaden eller tapet skyldes uaktsomhet eller brudd på denne avtale eller har skjedd på annen måte).
										<br><br>

										Du aksepterer å holde XLbil.no ikke ansvarlig for ethvert krav eller utgift som måtte oppstå som en følge av brudd på denne avtale av deg eller gjennom din XLbil.no-konto.
										<br><br>
									</li>
									<li>
										<strong>Lover og regler</strong><br>
										Alle juridiske forhold mellom involverte parter og XLbil.no skal behandles i henhold til norsk lov.
										<br><br>
									</li>
									<li>
										<strong>Annet</strong><br>
										For det tilfelle at noen bestemmelse i inneværende avtale skulle vise seg å være ugyldig eller ugjennomførlig grunnet rettslig kjennelse eller dom, skal gjenværende bestemmelser i avtalen fortsatt anses som gyldige og håndhevbare.
									</li>
								</ol>

							</article><!-- /.panel -->
						</div>
					</div>
				</div>
			</section>
		</main>
			<script>
				jQuery(document).ready(function() {
					if ( window.location.href !== "" ) {
						var target = window.location.hash.replace('#', "").replace('-panel', "");
						jQuery('.expandable-control[aria-controls="'+target+'"]').trigger('click');
					}
				});
			</script>
<?php require('footer.php'); ?>
