<?php
/**
 * Selg bil skjema
 * @package xlbil
 */
require('header.php');
?>
		<main>
			<section class="main-section">
				<div class="row">
					<h1>Selge bil</h1>
					<p>Fyll inn registreringsnummer for å hente inn informasjon om kjøretøyet.</p>
					
					<?php  
						$req_section = "(må fyllest ut)";
						$not_req_section = "(ikke påkrevd)";
					?>
					<form id="cardata" class="full-width" method="post" action="/selgbil/p/2473">
						<fieldset>
							<legend>Kjøretøyopplysninger</legend>
							<ul class="form-box by-three">
								<li><label for="attr_2055">Merke<input id="attr_2055" name="attr[2055][LATEXT]" value="VOLKSWAGEN" disabled="disabled" type="text"></label></li>
								<li><label for="attr_2071">Girkasse<input id="attr_2071" name="attr[2071][TEXT]" value="AUTOMAT" disabled="disabled" type="text"></label></li>
								<li><label for="attr_2058">Årsmodell<input id="attr_2058" name="attr[2058][INTEGER]" value="2013" disabled="disabled" type="number"></label></li>
								<li><label for="attr_2068">Modell<input id="attr_2068" name="attr[2068][LATEXT]" value="AMAROK" disabled="disabled" type="text"></label></li>
								<li><label for="attr_2064">Karosseri<input id="attr_2064" name="attr[2064][TEXT]" value="SEPARAT FØRERHUS" disabled="disabled" type="text"></label></li>
								<li><label for="attr_2067">Effekt<span class="unit-after"><input id="attr_2067" name="attr[2067][INTEGER]" value="132" disabled="disabled" type="number"><span>kW</span></span></label></li>
								<li><label for="attr_2063">C0<sub>2</sub><span class="unit-after"><input id="attr_2063" name="attr[2063][INTEGER]" value="203" disabled="disabled" type="number"><span>g/km</span></span></label></li>
								<li><label for="attr_2069">Første registreringsdato<input id="attr_2069" name="attr[2069][DATE]" value="01.02.2013" disabled="disabled" type="text"></label></li>
								<li><label for="attr_2088">Forrige EU kontroll<input id="attr_2088" name="attr[2088][DATE]" value="13.02.2017" disabled="disabled" type="text"></label></li>
								<li><label for="attr_2087">Neste EU kontroll<input id="attr_2087" name="attr[2087][DATE]" value="28.02.2019" disabled="disabled" type="text"></label></li>
								<li><label for="attr_2066">Antall seter<input id="attr_2066" name="attr[2066][INTEGER]" value="2" disabled="disabled" type="number"></label></li>
								<li><label for="attr_2060">Drivstoff<input id="attr_2060" name="attr[2060][TEXT]" value="Diesel" disabled="disabled" type="text"></label></li>
							</ul>
						</fieldset>

						<fieldset>
							<legend>Supplerende opplysninger (påkrevd)</legend>
							<p>Bruk feltet «Egen beskrivelse av kjøretøyet» hvis du har utfyllende informasjon.</p>
							<ul class="form-box by-three">
								<li><label for="attr_2057">Kjøretøyet står i kommune<input id="attr_2057" name="attr[2057][LATEXT]" value="" type="text"></label></li>
								<li><label for="attr_2065">Kilometerstand<input id="attr_2065" name="attr[2065][INTEGER]" value="" type="number"></label></li>
								<li><label for="attr_2085">Antall nøkler<input id="attr_2085" name="attr[2085][INTEGER]" value="" type="number"></label></li>
							</ul>
							
							<ul class="form-box by-three spaced-children-bottom">
								<li><label for="attr_2119">Er alle servicer fulgt?</label>
									<ul class="form-box inline">
										<li><label for="attr_2119_1" class="ui-radio"><input id="attr_2119_1" name="attr[2119][INTEGER]" value="1" type="radio"><span></span> Ja</label></li>
										<li><label for="attr_2119_0" class="ui-radio"><input id="attr_2119_0" name="attr[2119][INTEGER]" value="0" checked="checked" type="radio"><span></span> Nei</label></li>
										<li><label for="attr_2119_-1" class="ui-radio"><input id="attr_2119_-1" name="attr[2119][INTEGER]" value="-1" type="radio"><span></span> Vet ikke</label></li>
									</ul>
								</li>
								<li><label for="attr_2120">Er registerreim skiftet?</label>
									<ul class="form-box inline">
										<li><label for="attr_2120_1" class="ui-radio"><input id="attr_2120_1" name="attr[2120][INTEGER]" value="1" type="radio"><span></span> Ja</label></li>
										<li><label for="attr_2120_0" class="ui-radio"><input id="attr_2120_0" name="attr[2120][INTEGER]" value="0" checked="checked" type="radio"><span></span> Nei</label></li>
										<li><label for="attr_2120_-1" class="ui-radio"><input id="attr_2120_-1" name="attr[2120][INTEGER]" value="-1" type="radio"><span></span> Vet ikke</label></li>
									</ul>
								</li>
								<li><label for="attr_2121">Er bilen bruktimportert?</label>
									<ul class="form-box inline">
										<li><label for="attr_2121_1" class="ui-radio"><input id="attr_2121_1" name="attr[2121][INTEGER]" value="1" disabled="disabled" type="radio"><span></span> Ja</label></li>
										<li><label for="attr_2121_0" class="ui-radio"><input id="attr_2121_0" name="attr[2121][INTEGER]" value="0" checked="checked" disabled="disabled" type="radio"><span></span> Nei</label></li>
									</ul>
								</li>
								<li><label for="attr_2122">Har bilen vært skadet?</label>
									<ul class="form-box inline">
										<li><label for="attr_2122_1" class="ui-radio"><input id="attr_2122_1" name="attr[2122][INTEGER]" value="1" type="radio"><span></span> Ja</label></li>
										<li><label for="attr_2122_0" class="ui-radio"><input id="attr_2122_0" name="attr[2122][INTEGER]" value="0" checked="checked" type="radio"><span></span> Nei</label></li>
										<li><label for="attr_2122_-1" class="ui-radio"><input id="attr_2122_-1" name="attr[2122][INTEGER]" value="-1" type="radio"><span></span> Vet ikke</label></li>
									</ul>
								</li>
								<li><label for="attr_2123">Er det sommer- og vinterhjul på felg til bilen?</label>
									<ul class="form-box inline">
										<li><label for="attr_2123_1" class="ui-radio"><input id="attr_2123_1" name="attr[2123][INTEGER]" value="1" type="radio"><span></span> Ja</label></li>
										<li><label for="attr_2123_0" class="ui-radio"><input id="attr_2123_0" name="attr[2123][INTEGER]" value="0" checked="checked" type="radio"><span></span> Nei</label></li>
										<li><label for="attr_2123_-1" class="ui-radio"><input id="attr_2123_-1" name="attr[2123][INTEGER]" value="-1" type="radio"><span></span> Vet ikke</label></li>
									</ul>
								</li>
								<li><label for="attr_2124">Er det riper eller sprekker i frontruta?</label>
									<ul class="form-box inline">
										<li><label for="attr_2124_1" class="ui-radio"><input id="attr_2124_1" name="attr[2124][INTEGER]" value="1" type="radio"><span></span> Ja</label></li>
										<li><label for="attr_2124_0" class="ui-radio"><input id="attr_2124_0" name="attr[2124][INTEGER]" value="0" checked="checked" type="radio"><span></span> Nei</label></li>
										<li><label for="attr_2124_-1" class="ui-radio"><input id="attr_2124_-1" name="attr[2124][INTEGER]" value="-1" type="radio"><span></span> Vet ikke</label></li>
									</ul>
								</li>
								<li><label for="attr_2125">Har bilen bulker eller riper utvendig?</label>
									<ul class="form-box inline">
										<li><label for="attr_2125_1" class="ui-radio"><input id="attr_2125_1" name="attr[2125][INTEGER]" value="1" type="radio"><span></span> Ja</label></li>
										<li><label for="attr_2125_0" class="ui-radio"><input id="attr_2125_0" name="attr[2125][INTEGER]" value="0" checked="checked" type="radio"><span></span> Nei</label></li>
										<li><label for="attr_2125_-1" class="ui-radio"><input id="attr_2125_-1" name="attr[2125][INTEGER]" value="-1" type="radio"><span></span> Vet ikke</label></li>
									</ul>
								</li>
								<li><label for="attr_2126">Har interiøret synlig slitasje eller defekter?</label>
									<ul class="form-box inline">
										<li><label for="attr_2126_1" class="ui-radio"><input id="attr_2126_1" name="attr[2126][INTEGER]" value="1" type="radio"><span></span> Ja</label></li>
										<li><label for="attr_2126_0" class="ui-radio"><input id="attr_2126_0" name="attr[2126][INTEGER]" value="0" checked="checked" type="radio"><span></span> Nei</label></li>
										<li><label for="attr_2126_-1" class="ui-radio"><input id="attr_2126_-1" name="attr[2126][INTEGER]" value="-1" type="radio"><span></span> Vet ikke</label></li>
									</ul>
								</li>
								<li><label for="attr_2127">Er det dugg eller fukt i lykter?</label>
									<ul class="form-box inline">
										<li><label for="attr_2127_1" class="ui-radio"><input id="attr_2127_1" name="attr[2127][INTEGER]" value="1" type="radio"><span></span> Ja</label></li>
										<li><label for="attr_2127_0" class="ui-radio"><input id="attr_2127_0" name="attr[2127][INTEGER]" value="0" checked="checked" type="radio"><span></span> Nei</label></li>
										<li><label for="attr_2127_-1" class="ui-radio"><input id="attr_2127_-1" name="attr[2127][INTEGER]" value="-1" type="radio"><span></span> Vet ikke</label></li>
									</ul>
								</li>

							</ul>
					</fieldset>

					<fieldset>
					 <legend>Egen beskrivelse av kjøretøyet</legend>
					 <p>NB: Som selger er du forpliktet til å beskrive din bil så nøyaktig som mulig. Beskriv alle mangler og skader. Vi fraråder på det sterkeste til å holde tilbake mangler om bilen, dette kan få juridiske konsekvenser i ettertid. Vær ærlig! Mer informasjon om bilen øker muligheten for gode bud.</p>
					 <ul class="form-box">
					  <li><textarea id="cardata-description" name="attr[2118][TEXT]" maxlength="3500" rows="5" cols="70">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
					  </li>
					 </ul>
					</fieldset>

					<fieldset>
					 <legend>Bilder av kjøretøyet</legend>
					 <p>Det første bildet blir hovedbildet i auksjonen. Rengjør bilen grundig inn og utvendig å ta gode og tydelige bilder. Ta bilder av alle mangler og skader og servicehefte. Disse bildene laster du opp sist. Husk, gode bilder av kjøretøyet øker muligheten for salg.</p>
					 <ul class="form-box">
					  <li>
					   <div id="dz-image" class="dz-image-upload clear dz-clickable dz-started">
						<div class="dz-message">Trykk her for å laste opp bilder, eller bruk dra og slipp. Max 15 MB filstørrelse</div>
					   <div class="dz-preview dz-image-preview" id="1001930" data-orderkey="0">  <div class="dz-image"><img data-dz-thumbnail="" alt="Bilde 1" src="https://xlbil.no/i/dz/1001930-0.jpg"></div>  <div class="dz-details">    <div class="dz-size"><span data-dz-size=""><strong>0</strong> b</span></div>    <div class="dz-filename"><span data-dz-name="">Bilde 1</span></div>  </div>  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>  <div class="dz-error-message"><span data-dz-errormessage=""></span></div>  <div class="dz-success-mark">    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Check</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>      </g>    </svg>  </div>  <div class="dz-error-mark">    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Error</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>        </g>      </g>    </svg>  </div><a class="dz-remove" href="javascript:undefined;" data-dz-remove="">Fjern bildet</a></div></div>
					   <span id="image-container"><input multiple="multiple" class="dz-hidden-input" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;" type="file"></span>
					  </li>
					 </ul>
					</fieldset>

					<fieldset class="expandable">
					 <legend><button type="button" class="expandable-control" aria-expanded="false" aria-controls="documentation-form">NAF test/Tilstandsrapport</button></legend>
						  <div id="documentation-form" class="expandable-content" aria-hidden="true">
					  <p>Last opp NAF test eller tilstandsrapport. maks 6 MB</p>
					  <ul class="form-box">
					   <li>
						  <div id="dz-product-doc" class="dz-image-upload clear dz-clickable">
						   <div class="dz-message">Trykk her for å erstatte opplastet NAF test</div>
						  </div>
						  <span id="product-doc-container"><input class="dz-hidden-input" accept="application/pdf" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;" type="file"></span>
						 </li>
						</ul>
					   
					  
					 </div>
					</fieldset>

					<fieldset class="expandable">
					 <legend><button type="button" class="expandable-control" aria-expanded="false" aria-controls="equipment-box">Ekstrautstyr</button></legend>
						  <div id="equipment-box" class="expandable-content" aria-hidden="true">
					  <p>Velg ekstrautstyr</p>
					  <ul class="form-box by-four">
					   <li class="spaced">
						<label for="attr_2002" class="ui-check">Lasteholder / skistativ         <span>
						  <input id="attr_2002" name="attr[2002][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2003" class="ui-check">Luftfjæring         <span>
						  <input id="attr_2003" name="attr[2003][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2005" class="ui-check">Parkeringssensor bak         <span>
						  <input id="attr_2005" name="attr[2005][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2006" class="ui-check">Hengerfeste         <span>
						  <input id="attr_2006" name="attr[2006][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2009" class="ui-check">Diesel-partikelfilter         <span>
						  <input id="attr_2009" name="attr[2009][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2010" class="ui-check">Lettmetall-felger sommer         <span>
						  <input id="attr_2010" name="attr[2010][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2012" class="ui-check">Metallic lakk         <span>
						  <input id="attr_2012" name="attr[2012][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2013" class="ui-check">Sommerdekk         <span>
						  <input id="attr_2013" name="attr[2013][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2014" class="ui-check">Xenonlys         <span>
						  <input id="attr_2014" name="attr[2014][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2015" class="ui-check">Helårsdekk         <span>
						  <input id="attr_2015" name="attr[2015][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2016" class="ui-check">Lettmetall-felger vinter         <span>
						  <input id="attr_2016" name="attr[2016][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2018" class="ui-check">Nivåregulering         <span>
						  <input id="attr_2018" name="attr[2018][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2019" class="ui-check">Vinterdekk         <span>
						  <input id="attr_2019" name="attr[2019][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2020" class="ui-check">Parkeringssensor foran         <span>
						  <input id="attr_2020" name="attr[2020][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2022" class="ui-check">Bagasjeromstrekk         <span>
						  <input id="attr_2022" name="attr[2022][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2027" class="ui-check">DVD         <span>
						  <input id="attr_2027" name="attr[2027][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2029" class="ui-check">Elektriske speil         <span>
						  <input id="attr_2029" name="attr[2029][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2030" class="ui-check">Nøkkelløs lås         <span>
						  <input id="attr_2030" name="attr[2030][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2053" class="ui-check">Sentrallås         <span>
						  <input id="attr_2053" name="attr[2053][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2037" class="ui-check">Sportsseter         <span>
						  <input id="attr_2037" name="attr[2037][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2039" class="ui-check">TV         <span>
						  <input id="attr_2039" name="attr[2039][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2041" class="ui-check">Handsfree         <span>
						  <input id="attr_2041" name="attr[2041][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2042" class="ui-check">CD-spiller         <span>
						  <input id="attr_2042" name="attr[2042][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2043" class="ui-check">Elektrisk justerbare seter         <span>
						  <input id="attr_2043" name="attr[2043][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2044" class="ui-check">El. vinduer         <span>
						  <input id="attr_2044" name="attr[2044][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2045" class="ui-check">Multifunksjonsratt         <span>
						  <input id="attr_2045" name="attr[2045][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2046" class="ui-check">Radio DAB+         <span>
						  <input id="attr_2046" name="attr[2046][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2047" class="ui-check">Takluke         <span>
						  <input id="attr_2047" name="attr[2047][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2049" class="ui-check">Kjørecomputer         <span>
						  <input id="attr_2049" name="attr[2049][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2051" class="ui-check">Cruisecontrol         <span>
						  <input id="attr_2051" name="attr[2051][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2038" class="ui-check">Sotede vinduer         <span>
						  <input id="attr_2038" name="attr[2038][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2036" class="ui-check">Aircondition         <span>
						  <input id="attr_2036" name="attr[2036][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2035" class="ui-check">Navigasjonssystem         <span>
						  <input id="attr_2035" name="attr[2035][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2034" class="ui-check">Regnsensor         <span>
						  <input id="attr_2034" name="attr[2034][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2033" class="ui-check">Servostyring         <span>
						  <input id="attr_2033" name="attr[2033][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2032" class="ui-check">Skinninteriør         <span>
						  <input id="attr_2032" name="attr[2032][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2028" class="ui-check">Oppvarmede seter         <span>
						  <input id="attr_2028" name="attr[2028][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2026" class="ui-check">ABS-bremser         <span>
						  <input id="attr_2026" name="attr[2026][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2025" class="ui-check">Antispinn         <span>
						  <input id="attr_2025" name="attr[2025][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2024" class="ui-check">Startsperre         <span>
						  <input id="attr_2024" name="attr[2024][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2023" class="ui-check">Airbag foran         <span>
						  <input id="attr_2023" name="attr[2023][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2021" class="ui-check">Diff.sperre         <span>
						  <input id="attr_2021" name="attr[2021][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2017" class="ui-check">Stålbjelker         <span>
						  <input id="attr_2017" name="attr[2017][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2008" class="ui-check">Alarm         <span>
						  <input id="attr_2008" name="attr[2008][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2007" class="ui-check">Side aribag         <span>
						  <input id="attr_2007" name="attr[2007][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2004" class="ui-check">Antiskrens         <span>
						  <input id="attr_2004" name="attr[2004][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2104" class="ui-check">Kupevarmer         <span>
						  <input id="attr_2104" name="attr[2104][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2100" class="ui-check">Heads up display         <span>
						  <input id="attr_2100" name="attr[2100][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2098" class="ui-check">Bluetooth         <span>
						  <input id="attr_2098" name="attr[2098][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2000" class="ui-check">Avtagbart hengerfeste         <span>
						  <input id="attr_2000" name="attr[2000][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2092" class="ui-check">Setevarmer         <span>
						  <input id="attr_2092" name="attr[2092][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2102" class="ui-check">Isofix         <span>
						  <input id="attr_2102" name="attr[2102][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2096" class="ui-check">AUX-inngang         <span>
						  <input id="attr_2096" name="attr[2096][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					   <li class="spaced">
						<label for="attr_2094" class="ui-check">Soltak/glasstak         <span>
						  <input id="attr_2094" name="attr[2094][BOOL]" type="checkbox">
						  <span></span>
						 </span>
						</label>
					   </li>
					  </ul>
					 </div>
					</fieldset>


					<fieldset>
					 <ul class="form-box spaced-children">
					  <li>
					   <ul class="form-box by-three">
						<li><button type="submit" class="btn green" name="updpreview_x"><i class="fa fa-eye" aria-hidden="true"></i> Forhåndsvis og lagre</button></li>
						<li><button type="submit" class="btn blue" name="upd_x"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lagre</button></li>
						<li><a href="/vaccount" class="btn blue">Tilbake til min konto</a></li>
				<!--
						<li><a id="submit-cardata-form" class="btn blue" href="#"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Start auksjonen</a></li>
						<li><input type="submit" id="submit-cardata-form2" class="btn blue" name="publish_x" value=" Start auksjonen" disabled></li>
				-->
					   </ul>
					  </li>
					 </ul>
					</fieldset>
				   </form>
						
				</div>
			</section>
		</main>

<?php require('footer.php'); ?>
