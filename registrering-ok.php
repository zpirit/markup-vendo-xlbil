<?php
/**
 * Main index file fior xl-bil
 */
require('header.php');
?>
		<main>
			<section class="main-section">
				<div class="row">
					<h1>Takk! Du er nå registrert!</h1>
					<p>En e-post er sendt til deg med dine opplysninger, og en lenke du må klikke på for å aktivere kontoen din.</p>

					<a href="login.php" class="btn blue">Logg inn</a> <a href="index.php" class="btn blue">Gå til forsiden</a>
					
				</div>
			</section>
		</main>
<?php require('footer.php'); ?>
