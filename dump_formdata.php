<?php 
	if ( ! empty( $_FILES ) ) {
		foreach ( $_FILES as $file ) {
			file_put_contents( ".dumpfile", var_export( $file, true ) );
			move_uploaded_file( $file['tmp_name'], dirname(__FILE__) . '/assets/img/uploads/' . $file['name'] );
		}
	}
?>