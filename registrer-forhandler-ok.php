<?php
/**
 * Register user
 * @package xlbil
 */
require('header.php'); ?>
		
		<main>
			<section class="main-section">
				<div class="row">
					<h1>Velkommen til XLBil!</h1>
					<p>Ditt foretak verifiseres mot Brønnøysundsregistrene innen kort tid, senest i løpet av 24 timer.</p>
					<p>Godkjent brukerprofil bekreftes pr e-post.</p>
					<p>Tilbake til <a href="index.php">forsiden</a></p>
					
				</div>
			</section>
		</main>

<?php require('footer.php'); ?>
