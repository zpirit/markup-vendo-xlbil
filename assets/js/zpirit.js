var Zpirit_cookie = {
	/**
	 * Object init
	 */
	init: function() {
		this.cookieNotice();
	},
	cookieNotice: function() {
		if ( window.localStorage.getItem('hide-cookie-notice') === null ) {
			// no cookie preference is set by user show cookie notice
			this.cookieNoticeCreate();
		}
	},
	cookieNoticeCreate: function() {
		// div id root
		var croot = document.getElementById('root');
		if ( ! croot ) {
			croot = document.createElement('div');
			croot.id="root";
			document.body.appendChild(croot);
		}

		// create documentfragment
		var frag = document.createDocumentFragment();

		// create a div element
		var cdiv = document.createElement('div');
		cdiv.id = "cookie-notice";
		
		// create a paragraph
		var cpar = document.createElement('p');
		cpar.textContent = 'Vi bruker informasjonskapsler på nettsiden vår, fortsetter du å bruke xlbil.no godtar du dette. ';
		
		// create a link
		var clink = document.createElement('a');
		clink.href = 'https://xlbil.no/cookies';
		clink.textContent = 'Les mer';
		// append link to paragraph
		cpar.appendChild(clink);
		
		// append paragraph to div
		cdiv.appendChild(cpar);

		// creata a close button
		var cbut = document.createElement('button');
		cbut.id = "close-cookie-notice";
		cbut.addEventListener('click', this.cookieNoticeHide);
		
		// creata an fonticon element
		var cico = document.createElement('i');
		cico.className = "fa fa-close";
		
		// append fonticon to button
		cbut.appendChild(cico);
		
		// append button to div
		cdiv.appendChild(cbut);
		
		// append div to fragment
		frag.appendChild(cdiv);
		
		// create style element
		var cstyle = document.createElement('style');
		// append styles to root 
		croot.appendChild(cstyle);
		var csheet = cstyle.sheet;
		var rules = [
			[
				"#cookie-notice", 
				[
					"position: fixed;",
					"bottom: 0;",
					"left: 0;",
					"background: #0069b4;",
					"width: 100%;",
					"color: #ffffff;",
					"text-align: center;",
					"padding: 25px 3em;",
					"z-index: 99;"
				]
			],
			[
				"#cookie-notice p",
				[
					"margin: 0;"
				]
			],
			[
				"#cookie-notice a",
				[
					"color: #ffffff;",
					"font-weight: 700;",
					"font-style: italic;",
					"text-decoration: underline;"
				]
			],
			[
				"#cookie-notice button",
				[
					"position: absolute;",
					"top: 12px;",
					"background: none;",
					"right: 25px;",
					"font-size: 1.8em;",
					"color: #ffffff;",
					"width: 2em;",
					"height: 2em;",
					"border: none;",
					"outline: none;"
				]
			],
			[
				"@media all and (max-width: 991px)",
				[
					"#cookie-notice p { font-size: 1.2em; }",
					"#cookie-notice button { top: 6px; right: 6px; font-size: 1.4em; }",
				]
			],

		];
		for ( var i = 0; i < rules.length; i++ ) {
			var selector = rules[i][0];
			var props = "";
			for ( var r = 0; r < rules[i][1].length; r ++ ) {
				props += rules[i][1][r];
			}
			// append rules til stylesheet
			csheet.insertRule( selector + "{" + props + "}", csheet.cssRules.length);
		}
		
		// append fragment to root
		croot.appendChild(frag);

	},
	cookieNoticeHide: function() {
		if( window.localStorage ) 
			window.localStorage.setItem('hide-cookie-notice', "true");
		
		var cockNotice = document.getElementById('cookie-notice');
		if ( cockNotice )
			cockNotice.parentNode.removeChild(cockNotice);
	}
};