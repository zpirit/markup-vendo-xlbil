/**
 * main js functions
 * @package xlbil
 */
(function ($, root, undefined) {

	$(function () {

		'use strict';
		
		/**
		 * Dynamic Sticky Footer
		 */
		var wrapper = document.getElementById('wrapper');
		var footer = document.querySelector('.main-footer');
		function set_sticky_footer_size() {
			var footer_size = footer.getBoundingClientRect();
			wrapper.style.marginBottom = "-" + footer_size.height + "px";
			wrapper.style.paddingBottom = footer_size.height + 30 + "px";
		}
		// execute sticky footer
		set_sticky_footer_size();
		window.onresize = set_sticky_footer_size;

		// toggle mobile menu
		document.getElementById("toggleMainNav").addEventListener('click', function(event) {
			event.preventDefault();
			
			jQuery("body").toggleClass("no-scroll");
			jQuery(this).toggleClass("scramble");
			jQuery(".header-nav").toggleClass('active');
		});

		// switch logibn type, user or buyer
		jQuery(".form-switchers button").on('click', function(event) {
			if ( ! jQuery(this).hasClass("active") ) {
				jQuery(this).addClass("active").siblings().removeClass("active");
				jQuery(this.getAttribute('data-activate')).addClass("active").siblings().removeClass("active");
			}
		});

		// select2 on select fields
		jQuery('.select2-init').each(function() {
			$(this).select2({
				placeholder: $(this).attr('placeholder'),
			});
		});
		jQuery("#user-clear-subscriptions").on('click', function() {
			jQuery('#user_subscribed').val(null).trigger('change');
			// jQuery('#user_subscribe_to_notice').submit();
		});

		// Enable submit if approve conditions
		jQuery(".user-register-form").on('change', 'input[type="checkbox"]', function(event) {
			if ( document.getElementById("user-accept-terms").checked ) {
				document.getElementById("submit-user-form").removeAttribute('disabled');
			}
			else {
				document.getElementById("submit-user-form").setAttribute('disabled', 'disabled');
			}
		});
		// Enable submit if approve conditions
		jQuery("#register-company").on('change', 'input[type="checkbox"]', function(event) {
			if ( document.getElementById("company-accept-terms").checked && document.getElementById("company-accept-payment").checked && document.getElementById("company-accept-comittment").checked && document.getElementById("company-accept-approvement").checked ) {

				document.getElementById("submit-company-form").removeAttribute('disabled');
			}
			else {
				document.getElementById("submit-company-form").setAttribute('disabled', 'disabled');
			}
		});
		// Enable submit if approve conditions
		jQuery("#cardata").on('change', 'input[type="checkbox"]', function(event) {
			if ( document.getElementById("cardata-accept-terms").checked ) {
				document.getElementById("submit-cardata-form").removeAttribute('disabled');
			}
			else {
				document.getElementById("submit-cardata-form").setAttribute('disabled', 'disabled');
			}
		});
		
		// fetch zipcodes with locations
		var postcodes = false;
		if ( document.querySelector(".post-number-input") ) {
			jQuery.getJSON("assets/js/norwegian-form-data.json", function( json ) {
				postcodes = json.zipcodes;
			});
		}
		jQuery('.post-number-input').on('keyup', function(event) {
			var locEl = document.querySelector(".post-location");
			if ( postcodes && locEl ) {
				var val = this.value;
				// only get zipcode if correct length
				if ( val.length === 4 && val.match(/\d{4}/) ) {
					if ( postcodes.hasOwnProperty(val) ) {
						locEl.value = postcodes[val];
					}
					else {
						locEl.value = "";
					}
				}
			}
		});


		// ui-select list show dd
		jQuery('.ui-select input').on('focus', function() {
			jQuery(this).siblings("ul").addClass('active');
		});
		// set input value for ui-select-value
		jQuery('.ui-select button').on('click', function(){
			jQuery(this).parents('ul').prev().val(jQuery(this).text());
		});
		// filter ui-select-dd
		var timerId = null;
		jQuery('.ui-select input').on('keyup', function() {
			do_the_filter_stuff( this );
		});

		function do_the_filter_stuff( el ) {
			var value = jQuery(el).val();

			// clear timer if its running
			if ( timerId ) 
				clearTimeout( timerId );
			
			timerId = setTimeout( function() {
				var term = new RegExp( value );
				jQuery(el).next().children().css( 'display', 'block' );
				jQuery(el).next().children().filter( function( index ) {
					if ( jQuery(this).children().text().match( term ) === null )
						return true;
				}).css('display', 'none');
			}, 500);
		}

		// inint dropzone fileupload
		if ( document.getElementById( "cardata-image" ) ) {
			var myDropzone = new Dropzone("#cardata #cardata-image", {
				url: "dump_formdata.php",
				hiddenInputContainer: "#files-container",
				maxFilesize: 1,
				acceptedFiles: ".jpg,.png,.gif",
				addRemoveLinks: true,
				dictRemoveFile: "Fjern bildet",
				dictFileTooBig: "Bildet er for stort",
				renameFilename: function( name ) {
					var extension = name.match(/\.(jpg|png|gif)/);
					return "bil_" + Math.floor(Date.now() + Math.random()) + extension[0];
				}
			});
		}
		// inint dropzone fileupload
		if ( document.getElementById( "cardata-declaration-naf" ) ) {
			var myNextDropzone = new Dropzone("#cardata #cardata-declaration-naf", {
				url: "dump_formdata.php",
				hiddenInputContainer: "#cardata-naf-container",
				maxFilesize: 1,
				acceptedFiles: ".pdf",
				addRemoveLinks: true,
				dictRemoveFile: "Fjern NAF test",
				dictFileTooBig: "Dokumentet er for stort",
				renameFilename: function( name ) {
					var extension = name.match(/\.pdf/);
					return "naftest_" + Math.floor(Date.now() + Math.random()) + extension[0];
				}
			});
		}

		// expand fieldset in form
		jQuery('.expandable-control').on('click', function(e) {
			var expandable = document.getElementById(this.getAttribute('aria-controls'));
			if ( this.getAttribute('aria-expanded') === "false" ) {
				// expandable.style.minHeight = expandable.scrollHeight + "px";
				this.setAttribute('aria-expanded', 'true');
				jQuery(expandable).addClass('expanded').attr('aria-hidden', 'false');
			}
			else {
				// expandable.style.height = "0px";
				this.setAttribute('aria-expanded', 'false');
				jQuery(expandable).removeClass('expanded').attr('aria-hidden', 'true');
			}
		});

		// setup countdown on car detail page
		var timeleft_parent = jQuery('.remaining-time'),
			timeleft_days = jQuery('#remaining-time-days'),
			timeleft_hours = jQuery('#remaining-time-hours'),
			timeleft_minutes = jQuery('#remaining-time-minutes'),
			timeleft_seconds = jQuery('#remaining-time-seconds');

		timeleft_parent.countdown( timeleft_parent.data('endtime'), function(event) {
			timeleft_days.text( event.strftime('%D') );
			timeleft_hours.text( event.strftime('%H') );
			timeleft_minutes.text( event.strftime('%M') );
			timeleft_seconds.text( event.strftime('%S') );
		}).on('finish.countdown', function(event) {
			timeleft_parent.children().remove();
			timeleft_parent.append('<span class="countdown-finished">Avsluttet auksjon</span>');
			jQuery("#pre-bid").remove();
		});
		
		// show filter select
		jQuery('.filter-item .filter-selected').on('click', function(event) {
			event.preventDefault();
			jQuery('.filter-item ul').not(jQuery(this).siblings('ul')).removeClass('active');
			jQuery(this).siblings('ul').toggleClass('active');
		});

		// set active sort choise
		jQuery('.filter-item ul li a').on('click', function(event) {
			event.preventDefault();
			var $list = jQuery( this.parentNode.parentNode );
			$list.siblings('.filter-selected').text( this.innerText );
			$list.siblings('input').val( this.getAttribute('data-value') ).change();
			$list.removeClass('active');
		});
		
		// listen for changes to filters
		jQuery('.filters input').on('change', function(event) {
			// do the filtering
			console.log( "Sort or filter by input: " + this.name + " with value: " + this.value );
		});

		// inint flexslider on car detail page carousel load first
		$('#carousel').flexslider({	
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			prevText: "",
			nextText: "", 
			itemWidth: 123, 
			itemMargin: 5, 
			asNavFor: "#slider"
		});
		$('#slider').flexslider({	
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			sync: "#carousel"
		});
		
		// set height on slider li
		// var detail_slider = jQuery('.car-detail-slider');
		// var slider_li = jQuery("#slider .slides li");
		
		// var carousel_li = jQuery("#carousel .slides li");
		// carousel_li.height( ((detail_slider.width() * 0.8) / 4 + 7) / 1.77 );
		
		// function set_slider_height() {
		// 	slider_li.height( detail_slider.width() / 1.77 );
		// }
		// set_slider_height();
		// window.onresize = set_slider_height;

		var overlay = jQuery('.overlay');
		jQuery('.overlay-trigger').on( 'click', function(event) {
			event.preventDefault();
			var ol = jQuery(this).data('overlay');
			if ( typeof ol === "object" ) {
				var $olw = jQuery( ol.id );
				var $sc = $olw.find('.service-caller');
				$sc[0].href = $sc[0].href.replace('{carid}', ol.carid);
				
				$olw.addClass('active');
			}
			else {
				jQuery( this.getAttribute('data-overlay') ).addClass('active');
			}
		});

		function overlay_close(el) {
			el.removeClass('active');
		}

		// etter validering
		jQuery("#pre-bid").on('submit', function(event) {
			event.preventDefault();
			
			var userBid = jQuery('#user-bid').val();
			var userAutoBid = jQuery('#user-autobid').val();

			//do some validation the way you like
			if ( userBid && userAutoBid ) { 
				jQuery("#bidconfirm-bid").text(userBid);
				jQuery("#verified-bid-bid").val(userBid);
				
				jQuery("#bidconfirm-autobid").text(userAutoBid);
				jQuery("#verified-bid-autobid").val(userAutoBid);
				
				overlay.toggleClass('active');
			}
		});

		// close overlay
		jQuery('.overlay-destroy').on('click', function(event) {
			jQuery(this).parents('.overlay').toggleClass('active');
		});

		/**
		 * CTABS
		 */
		jQuery('.ctabs-tabs li a').on('click', function(event) {
			event.preventDefault();

			var tab = this.getAttribute('href');

			jQuery(this).addClass('active').parent('li').siblings().children().removeClass('active');

			jQuery('.ctabs-content ' + tab).fadeIn(400).siblings().hide();
		});

		// Close elements on click outside element
		jQuery(document).on("click touchstart", function(event) {
			// Close megamenu and who-menu
			if( jQuery(event.target).closest(".ui-select").length === 0 ) {
				jQuery('.ui-select ul').removeClass('active');
			}
			// if( jQuery(event.target).closest(".viewport").length === 0 ) {
			// 	overlay.removeClass('active');
			// }
		});
		// Close elements on escape keyup
		jQuery(document).on('keyup', function(event) {
			if ( event.keyCode === 27 ) {
				jQuery('.ui-select ul').removeClass('active');
				overlay.removeClass('active');
			}
		});

		/**
		 * Server responses
		 */
		// global element for holding server responses
		var r_wrap = jQuery('#server-responses');

		// example object returned from ajax request
		var ex_resp = {
			responses: [
				{
					type: 'error',
					output: "Ein feilmelding som er alvorlig."
				},
				{
					type: 'warning',
					output: "Ein advarsel eller tilbakemelding."
				},
				{
					type: 'success',
					output: "Ein bekreftelse på at dettte går bra og at livet er ålreit."
				}
			]
		};

		// Trigger the event serverresponse with object as parameter
		jQuery('.mytrigger').on('click', function() {
			// the trigger with response object as param
			r_wrap.trigger('serverresponse', ex_resp);
			
			// just destroy the ugly trigger button
			jQuery(this).remove();
		});
		
		// custom event for displaying serverrors
		r_wrap.on('serverresponse', function(event, obj) {
			// display block on parent
			r_wrap.show();

			// only proceede if reponse is object
			if ( typeof obj !== 'object' ) 
				return;

			// check if object has reponses memeber
			if ( ! obj.hasOwnProperty('responses') )
				return;

			// loop over responses
			for ( var i = 0; i < obj.responses.length; i++ ) {
				var r = obj.responses[i];
				var el = '<li><aside class="response '+ r.type +'">'+ r.output +' <button type="button" class="btn remove-response"><i class="fa fa-times" aria-hidden="true"></i></button></aside></li>';
				r_wrap.append(el);
			}

			// incremental delay on slide in
			r_wrap.find('li').each( function(index){
				jQuery(this).delay(index * 100).animate({left: 0}, 1000);
			});
		});
		
		// closing displayed responses
		r_wrap.on('click', '.remove-response', function() {
			jQuery(this).parents('li').animate({
					left: '-100%'
				}, 
				500, 
				function() {
					jQuery(this).remove();
					if ( ! r_wrap.children().length ) {
						r_wrap.hide();
					}
			});
		});

		/**
		 * Selg-bil - Validate that this is my car
		 */
		jQuery("#cardata-reginfo").on('submit', function(event) {
			event.preventDefault();
			var frm = this;
			var step1 = document.querySelector('.step-1');
			var el = jQuery(".its-my-car");
			
			if ( jQuery("#cardata-regid").val() === "" || jQuery("#cardata-regid").val().length !== 7  ) {
				el.addClass('active');
				el.find('h2').html("Prøv med et skiltnummer, bruk 7 tegn");
				el.find('h1').html("");
				return;
			}
			else {
				// or a post request to input data api
				jQuery.ajax({
					url: 'assets/js/inputdata-response.json',
					method: 'GET',
					dataType: 'json',
				}).fail(function( jqXHR, textStatus ) {
					console.log('AJAX ERROR - Could not retreive json response.');
				}).done(function( data ) {
					if ( typeof data == "object" ) {
						step1.parentNode.removeChild(step1);

						el.addClass('active');
						el.find('h2').html("Er dette bilen du vil selge?");
						el.find('h1').html(data.merkeNavn + " " + data.modellbetegnelse + ", " +  data.kjennemerke + ", " + data.regAAr +" modell");
						el.append('<div class="spacer"></div><a class="btn green spaced" href="selg-bil.php">Ja, denne bilen vil jeg selge</a>&nbsp;<a class="btn red" href="selg-bil-steg1.php">Avbryt</a>');
					}
				});
			}
		});


		// format numbers in car details page, bid and autobid
		jQuery("#user-bid").focus( function(){
			jQuery(this).select();
		}).mouseup( function(e){
			// fix for webkit browsers
			e.preventDefault();
		});

		jQuery("#user-autobid").focus( function(){
			jQuery(this).select();
		}).mouseup( function(e){
			// fix for webkit browsers
			e.preventDefault();
		});

		jQuery("#user-bid").on('keyup', function(){
			parse_formatter(this);
		});
		jQuery("#user-autobid").on('keyup', function(){
			parse_formatter(this);
		});
		function parse_formatter(dat) {
			var $dat = jQuery(dat);
			if ( $dat.val() === '' ) {
				dat.setAttribute('value', '');
				return;
			}
			else {
				var n = parseInt( $dat.val().replace(/\D/g,''), 10);
				if ( isNaN(n) ) {
					$dat.val('');
					dat.setAttribute('value', '');
				}
				else {
					dat.setAttribute('value', n);
					$dat.val(n.toLocaleString('nb-NO'));
				}
			}
		}

		// start cookie notice
		Zpirit_cookie.init();
	});

})(jQuery, this);
