# README #

Markup for XLBIl web site, coop between Vendo and Zpirit.

### What is this repository for? ###

* Markup and styles for use by Vendo auction system
* V0.6

### How do I get set up? ###

* Clone locally.
* run `npm install --save-dev` inside folder in terminal.
* run `gulp` in terminal to start developing.

### Contribution guidelines ###

* Use scss for styles in assets/css/style.scss
* Run `gulp` to start browsersync and auto compile scss in assets/css/style.scss, and to minify js in assets/js/author.js.
* Run `gulp watch` to start autocompiling of scss and js without browsersync.
* Place vendor script files in assets/js/vendor-src and run `gulp vendor` to minify an combine js vendor files.
* Push to master branch to deploy to lab.zpirit.no/xl-bil/ if you are on the allowed user list.
* Suggest edits in a new branch for each suggestion.
* Create pull requests, or just merge it yourself with master.

### Whats missing

* Auction details
* Min side
* Add new car form
* Help and about pages

### Who do I talk to? ###

* Zpirit developer Øyvind <oyvind@zpirit.no>
* Zpirit developer Espen <espen@zpirit.no>
* Vendo developer Ola <nordmann@vendo.no>