<?php  
/**
 * Footer HTML
 * @package xlbil
 */
?>	</div><!-- End #wrapper -->
		<footer class="main-footer">
			<div class="row clear">
				<div class="grid60">
					<div class="secondary-logo">
						<a href="/xlbil">
							<img src="assets/img/xl-bil-logo.svg" alt="XLBil">
						</a>
					</div>

					<address>
						<strong>XL BIL AS</strong> &mdash; 
						Ringvegen 30, 5412 Stord &mdash; 
						E-post: <a href="mailto:support@xlbil.no">support@xlbil.no</a> &mdash; 
						Org.nr: NO 912 471 209 MVA
					</address>
				</div>
				<div class="grid40 secondary-info">
					<div class="social-link"><a href="https://facebook.com/"><i class="fa fa-facebook"></i> Følg oss på Facebook for å få med deg unike tilbud og auksjoner!</a></div>
					<div class="cookie-link"><a href="om-oss.php#about-terms-panel"> informasjon om cookies</a></div>
				</div>
			</div>
		</footer>
	</body>
</html>