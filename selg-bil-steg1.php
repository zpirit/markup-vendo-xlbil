<?php
/**
 * Selg bil skjema
 * @package xlbil
 */
require('header.php');
?>
		<main>
			<section class="main-section">
				<div class="row">
					<div class="step-1">
						<h1>Selge bil</h1>
						<p>Fyll inn registreringsnummer for å hente inn informasjon om kjøretøyet.</p>
						
						<?php  
							$req_section = "(må fyllest ut)";
							$not_req_section = "(ikke påkrevd)";
						?>
						<form id="cardata-reginfo" class="full-width" method="post" action="" enctype="multipart/form-data">
							<fieldset>
								<legend>Registreringsnummer</legend>
								<ul class="form-box by-three">
									<li>
										<input type="text" id="cardata-regid" name="cardata-regid" maxlength="7" placeholder="7 tegn" />
										<button type="submit" id="cardata-fetch-reginfo" class="btn blue">Hent opplysninger</button>
									</li>
								</ul>
							</fieldset>

						</form>
					</div>	
					<div class="its-my-car">
						<h2>Er dette bilen din?</h2>
						<h1></h1>
					</div>
						
				</div>
			</section>
		</main>

<?php require('footer.php'); ?>
