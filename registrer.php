<?php
/**
 * Register user
 * @package xlbil
 */
require('header.php'); ?>
		
		<main>
			<section class="main-section">
				<div class="row">
					<h1>Registrer deg</h1>
					<p><strong>Både privatpersoner og foretak kan registrere seg som selger, men kun registrerte bilhandlere kan registrere seg som kjøpere/budgivere.</strong></p>
					<p>Velg kontotype:</p>
					
					<?php  
						$btnClassUser = in_array($_GET['register'], array('user', 'user-private', 'user-company')) ? "blue" : "grey";
						$btnClassBuyer = $_GET['register'] === "buyer" ? "blue" : "grey";

						$btnClassUserPrivate = $_GET['register'] === "user-private" ? "blue" : "grey";
						$btnClassUserCompany = $_GET['register'] === "user-company" ? "blue" : "grey";
					?>
					<div class="btn-row">
						<a href="<?php echo $_SERVER['PHP_SELF']; ?>?register=user" class="btn min-200 <?php echo $btnClassUser; ?>">Selger</a>
						<a href="<?php echo $_SERVER['PHP_SELF']; ?>?register=buyer" class="btn min-200 <?php echo $btnClassBuyer; ?>">Kjøper (bilhandler)</a>
					</div>

					<?php  
						if ( ! empty( $_GET['register'] ) ) :
							if ( in_array($_GET['register'], array('user', 'user-private', 'user-company') ) ) : ?>
								<p>Selgertype:</p>
								<div class="btn-row">
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?register=user-private" class="btn min-200 <?php echo $btnClassUserPrivate; ?>">Privatperson</a>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?register=user-company" class="btn min-200 <?php echo $btnClassUserCompany; ?>">Firma</a>
								</div>
							<?php endif; ?>
							
							<?php if ( $_GET['register'] === "user-private" ) : ?>

									<form id="register-user-private" class="full-width user-register-form">
										<fieldset>
											<legend>Personopplysninger</legend>
											<ul class="form-box by-two">
												<li>
													<label for="user-name">Navn 
														<input type="text" class="form-control required" id="user-name" name="user-name" maxlength="100" required>
													</label>
												</li>
											</ul>
										</fieldset>

										<fieldset>
											<legend>E-post og passord</legend>
											<ul class="form-box by-two">
												<li>
													<label for="user-email">Brukernavn (e-post) 
														<input type="email" class="form-control required validate-email" id="user-email" name="user-email" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-email-confirm">Bekreft e-post 
														<input type="email" class="form-control required validate-email" id="user-email-confirm" name="user-email-confirm" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-password">Passord
														<input type="text" class="form-control required" id="user-password" name="user-password" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-password-confirm">Bekreft Passord
														<input type="text" class="form-control required" id="user-password-confirm" name="user-password-confirm" maxlength="100" required>
													</label>
												</li>
											</ul>
										</fieldset>

										<fieldset>
											<legend>Adresse og telefon</legend>
											<ul class="form-box by-two">
												<li>
													<label for="user-address">Adresse 
														<input type="text" class="form-control required" id="user-address" name="user-address" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-address-2">Adresse 2
														<input type="text" id="user-address-2" name="user-address-2" maxlength="100">
													</label>
												</li>

												<li>
													<label for="user-postnum">Postnummer
														<input type="text" class="form-control required validate-number" id="user-postnum" class="post-number-input" name="user-postnum" required>
													</label>
												</li>

												<li>
													<label for="user-location">Sted
														<input type="text" class="form-control required" id="user-location" class="post-location" name="user-location" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-country">Land
														<span class="select2-init">
															<select id="user-country" name="user-country">
																<option></option>
																<?php 
																	$formdata = file_get_contents( 'assets/js/norwegian-form-data.json' );
																	if ( $formdata ) {
																		$json = json_decode( $formdata );
																		foreach ( $json->countries as $code => $country ) {
																			echo sprintf( '<option value="%s">%s</option>', $code, $country );
																		}
																	}
																?>
															</select>
														</span>
													</label>
												</li>

												<li>
													<label for="user-telephone">Telefon/mobilnummer
														<input type="tel" class="form-control required validate-number" id="user-telephone" name="user-telephone" maxlength="100" required>
													</label>
												</li>
											</ul>
										</fieldset>

										<fieldset>
											<ul class="form-box">
												<li class="spaced">
													<label for="user-accept-terms" class="ui-check">Jeg har lest, og aksepterer <a href="om_oss.php">vilkår</a> for bruk av tjenesten.
														<span>
															<input type="checkbox" id="user-accept-terms" name="user-accept-terms">
															<span></span>
														</span> 
													</label>
												</li>
												<li class="spaced">
													<label for="user-accept-newsletter" class="ui-check">Jeg vil gjerne motta informasjon pr. e-post
														<span>
															<input type="checkbox" id="user-accept-newsletter" name="user-accept-newsletter">
															<span></span>
														</span> 
													</label>
												</li>
												<li class="spaced">
													<input type="submit" id="submit-user-form" value="Bekreft og registrer deg" disabled>
												</li>
											</ul>
										</fieldset>
									</form>
								
							<?php elseif ( $_GET['register'] === "user-company" ) : ?>
	
									<form id="register-user-company" class="full-width user-register-form">

										<fieldset>
											<legend>Firmaopplysninger</legend>
											<ul class="form-box by-two">
												<li>
													<label for="company-name">Firmanavn 
														<input type="text" class="form-control required" id="company-name" name="company-name" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="company-person">Kontaktperson 
														<input type="text" class="form-control required" id="company-person" name="company-person" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="company-id">Organisasjonsnummer
														<input type="text" class="form-control required" id="company-id" name="company-id" maxlength="100" required>
													</label>
												</li>
											</ul>
										</fieldset>

										<fieldset>
											<legend>E-post og passord</legend>
											<ul class="form-box by-two">
												<li>
													<label for="user-email">Brukernavn (e-post) 
														<input type="text" class="form-control required validate-email" id="user-email" name="user-email" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-email-confirm">Bekreft e-post 
														<input type="text" class="form-control required validate-email" id="user-email-confirm" name="user-email-confirm" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-password">Passord
														<input type="text" class="form-control required" id="user-password" name="user-password" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-password-confirm">Bekreft Passord
														<input type="text" class="form-control required" id="user-password-confirm" name="user-password-confirm" maxlength="100" required>
													</label>
												</li>
											</ul>
										</fieldset>

										<fieldset>
											<legend>Adresse og telefon</legend>
											<ul class="form-box by-two">
												<li>
													<label for="user-address">Adresse 
														<input type="text" class="form-control required" id="user-address" name="user-address" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-address-2">Adresse 2
														<input type="text" id="user-address-2" name="user-address-2" maxlength="100">
													</label>
												</li>

												<li>
													<label for="user-postnum">Postnummer
														<input type="text" id="user-postnum" class="post-number-input" name="user-postnum" required>
													</label>
												</li>

												<li>
													<label for="user-location">Sted
														<input type="text" id="user-location" class="post-location" name="user-location" maxlength="100" required>
													</label>
												</li>

												<li>
													<label for="user-country">Land
														<span class="select2-init">
															<select id="user-country" name="user-country">
																<option></option>
																<?php 
																	$formdata = file_get_contents( 'assets/js/norwegian-form-data.json' );
																	if ( $formdata ) {
																		$json = json_decode( $formdata );
																		foreach ( $json->countries as $code => $country ) {
																			echo sprintf( '<option value="%s">%s</option>', $code, $country );
																		}
																	}
																?>
															</select>
														</span>
													</label>
												</li>

												<li>
													<label for="user-telephone">Telefon/mobilnummer
														<input type="tel" id="user-telephone" name="user-telephone" maxlength="100" required>
													</label>
												</li>
											</ul>
										</fieldset>

										<fieldset>
											<ul class="form-box">
												<li class="spaced">
													<label for="user-accept-terms" class="ui-check">Jeg har lest, og aksepterer <a href="om_oss.php">vilkår</a> for bruk av tjenesten.
														<span>
															<input type="checkbox" id="user-accept-terms" name="user-accept-terms">
															<span></span>
														</span> 
													</label>
												</li>
												<li class="spaced">
													<label for="user-accept-newsletter" class="ui-check">Jeg vil gjerne motta informasjon pr. e-post
														<span>
															<input type="checkbox" id="user-accept-newsletter" name="user-accept-newsletter">
															<span></span>
														</span> 
													</label>
												</li>
												<li class="spaced">
													<input type="submit" id="submit-user-form" value="Bekreft og registrer deg" disabled>
												</li>
											</ul>
										</fieldset>
									</form>
							
							<?php endif; ?>

							<?php if ( $_GET['register'] === "buyer" ) : ?>
									
								<form id="register-company" class="register-form full-width">
									<fieldset>
										<legend>Forhandlerkonto: Firmaopplysninger</legend>
										<ul class="form-box by-two">
											<li>
												<label for="company-name">Firmanavn 
													<input type="text" class="form-control required" id="company-name" name="company-name" maxlength="100" required>
												</label>
											</li>

											<li>
												<label for="company-person">Kontaktperson 
													<input type="text" class="form-control required" id="company-person" name="company-person" maxlength="100" required>
												</label>
											</li>

											<li>
												<label for="company-id">Organisasjonsnummer
													<input type="text" class="form-control required" id="company-id" name="company-id" maxlength="100" required>
												</label>
											</li>
										</ul>
									</fieldset>

									<fieldset>
										<legend>E-post og passord</legend>
										<ul class="form-box by-two">
											<li>
												<label for="company-email">Brukernavn (e-post) 
													<input type="text" class="form-control required validate-email" id="company-email" name="company-email" maxlength="100" required>
												</label>
											</li>

											<li>
												<label for="company-email-confirm">Bekreft e-post 
													<input type="text" class="form-control required validate-email" id="company-email-confirm" name="company-email-confirm" maxlength="100" required>
												</label>
											</li>

											<li>
												<label for="company-password">Passord
													<input type="text" class="form-control required" id="company-password" name="company-password" maxlength="100" required>
												</label>
											</li>

											<li>
												<label for="company-password-confirm">Bekreft Passord
													<input type="text" class="form-control required" id="company-password-confirm" name="company-password-confirm" maxlength="100" required>
												</label>
											</li>
										</ul>
									</fieldset>

									<fieldset>
										<legend>Adresse og telefon</legend>
										<ul class="form-box by-two">
											<li>
												<label for="company-address">Adresse 
													<input type="text" class="form-control required" id="company-address" name="company-address" maxlength="100" required>
												</label>
											</li>

											<li>
												<label for="company-address-2">Adresse 2
													<input type="text" id="company-address-2" name="company-address-2" maxlength="100">
												</label>
											</li>

											<li>
												<label for="company-postnum">Postnummer
													<input type="text" id="company-postnum" class="post-number-input" name="company-postnum" required>
												</label>
											</li>

											<li>
												<label for="company-location">Sted
													<input type="text" id="company-location" class="post-location" name="company-location" maxlength="100" required>
												</label>
											</li>

											<li>
												<label for="company-country">Land
													<span class="select2-init">
														<select id="company-country" name="company-country">
															<option></option>
															<?php 
																$formdata = file_get_contents( 'assets/js/norwegian-form-data.json' );
																if ( $formdata ) {
																	$json = json_decode( $formdata );
																	foreach ( $json->countries as $code => $country ) {
																		echo sprintf( '<option value="%s">%s</option>', $code, $country );
																	}
																}
															?>
														</select>
													</span>
												</label>
											</li>

											<li>
												<label for="company-telephone">Telefon/mobilnummer
													<input type="tel" id="company-telephone" name="company-telephone" maxlength="100" required>
												</label>
											</li>
										</ul>
									</fieldset>

									<fieldset>
										<ul class="form-box spaced-children">
											<li>
												<label for="company-accept-terms" class="ui-check">Jeg har lest, og aksepterer <a href="om_oss.php">vilkår</a> for bruk av tjenesten.
													<span>
														<input type="checkbox" id="company-accept-terms" name="company-accept-terms" value="0">
														<span></span>
													</span> 
												</label>
											</li>
											<li>
												<input type="submit" id="submit-company-form" value="Bekreft og registrer deg" disabled>
											</li>
										</ul>
									</fieldset>
								</form>

							<?php 
							endif; 
						endif; 
					?>
				</div>
			</section>
		</main>

<?php require('footer.php'); ?>
