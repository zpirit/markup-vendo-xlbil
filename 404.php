<?php
/**
 * Main index file fior xl-bil
 */
require('header.php'); ?>
		<main>
			<section class="main-section">
				<div class="row">
					
					<article id="post-404" class="clear">
						<div class="">
							<h1>Oops, 404 feil!</h1>
							<p>Lenken du brukte for å komme hit virker ikke.</p>
							
							<?php if ( isset( $_SERVER['HTTP_REFERER'] ) ) : ?>
								<p><a href="<?php echo $_SERVER['HTTP_REFERER']; ?>">< Tilbake</a></p>
							<?php endif; ?>

							<p><a href="/">< Gå til fremsiden</a></p>
							
							<p>Du kan og prøve søkefeltet øverst på siden for å finne det du leter etter..</p>
						</div>

					</article><!-- /article -->
					
				</div>
			</section>
		</main>

<?php require('footers.php'); ?>
