<?php  
/**
 * Header file
 * @package xlbil
 */
require('functions.php');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="no" lang="no">
	<head>
		<meta charset="UTF-8">
		<title>XLBil - Gratis 7 dagers bilauksjon på nett.</title>
		<meta name="description" content="La bilhandlere by på din bruktbil, helt gratis!">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

		<?php if ( $_SERVER['HTTP_HOST'] === "xlbil.no" ) : ?>
		<link rel="canonical" href="https://xlbil.no">
		<?php endif; ?>
		<link rel="apple-touch-icon" sizes="57x57" href="assets/img/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="assets/img/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="assets/img/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="assets/img/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="assets/img/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="assets/img/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="assets/img/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="assets/img/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="assets/img/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="assets/img/manifest.json">
		<link rel="mask-icon" href="assets/img/pinned-safari-icon.svg" color="#0069b4">
		<meta name="msapplication-TileColor" content="#0069b4">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
		<link rel="stylesheet" href="style.css" media="all">

		<script src="assets/js/vendor.js?v=235"></script>
		<script src="assets/js/author.js"></script>

		<!-- Twitter Card data -->
		<meta name="twitter:card" content="summary">
		<meta name="twitter:title" content="XLBil - Gratis 7 dagers bilauksjon på nett.">
		<meta name="twitter:description" content="La bilhandlere by på din bruktbil, helt gratis!">
		<meta name="twitter:creator" content="zpiritas">
		<meta name="twitter:image" content="https://xlbil.no/assets/img/xlbil-social.jpg">
		<!-- Open Graph data -->
		<meta property="og:url" content="https://xlbil.no/" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="XLBil - Gratis 7 dagers bilauksjon på nett." />
		<meta property="og:description" content="La bilhandlere by på din bruktbil, helt gratis!" />
		<meta property="og:image" content="https://xlbil.no/assets/img/xlbil-social.jpg" />
		<meta property="og:site_name" content="XLBil" />

		<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "LocalBusiness",
				"address": { 
					"@type" : "PostalAddress", 
					"addressCountry" : "Norway", 
					"addressLocality" : "Stord", 
					"postalCode" : "5416", 
					"streetAddress" : "Hystadvegen 178"
				},
				"description": "XLBil er en norsk online nettauksjon hvor privatpersoner og foretak gratis kan avertere sine bruktbiler for salg til registrerte bilforhandlere og oppkjøpere i Norge. Bilhandlere registrerer seg hos XLBil.no for tilgangskonto og budgivning.",
				"name": "XLBil",
				"email": "support@xlbil.no",
				"logo": "https://xlbil.no/assets/img/xl-bil-logo.svg",
				"image": "https://xlbil.no/assets/img/xl-bil-gb.png",
				"url": "https://xlbil.no",
				"sameAs": "https://www.facebook.com/xlbil/"
			}
		</script>
 	</head>
 	

	<body itemscope itemtype="http://schema.org/WebPage">

		<div id="wrapper">
		
			<header class="main-header">
				<div class="row clear">
					<a id="toggleMainNav" href="#"><span><span></span><span></span><span></span></span></a>
					
					<a class="grid16 main-logo" href="/xlbil">
						<img src="assets/img/xl-bil-logo.svg" alt="XLBil">
					</a>
					
					<nav class="grid83 header-nav">
						<ul class="mobile-cookies">
							<li><a href="/cookies">Informasjon om cookies</a></li>
						</ul>
						<ul class="top-nav">
							<li class="active"><a href="om-oss.php#about-faq-panel">Hjelp/info <i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
							<li><a href="registrer.php">Registrer <i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>
							<li><a href="logg-inn.php">Logg inn <i class="fa fa-user-circle-o" aria-hidden="true"></i></a></li>
						</ul>
						<div class="main-nav-field">
							<ul class="grid40 search-nav">
								<li>
									<div class="search-form-wrapper">
										<form class="main-search-form" action="alle-auksjoner.php">
											<input type="search" name="q" placeholder="Søk etter merke, modell...">
											<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
										</form>
									</div>
								</li>
							</ul>

							<ul class="grid60 main-nav">
								<li class="active"><a href="alle-auksjoner.php">Alle auksjoner</a></li>
								<li><a href="om-oss.php#about-us-panel">Om XLBil</a></li>
								<li><a href="om-oss.php#about-terms-panel">Selge på XLBil</a></li>
							</ul>

						</div>
					</nav>
				</div>
			</header>

			<?php if ( 'index.php' === basename($_SERVER['PHP_SELF']) ) : ?>
				<section class="top-bar">
					<div class="row clear">
						<div class="grid60">
							<h1>Gratis 72 timers bilauksjon!</h1>
							<p>Motta bud fra profesjonelle bilhandlere. Start ved å skrive inn ditt registreringsnummer:</p>
						</div>
						<div class="grid40 clear">
							<div class="grid60">
								<div class="plate-number">
									<span class="plate-blue-bar">
										<!-- Heia Norge -->
										<img src="assets/img/heia_norge.svg" alt="Norsk flagg" />
										<span class="capital-country">N</span>
									</span>
									<form method="get">
										<input type="text" id="skiltnummer" name="skiltnummer" placeholder="XX 00000" />
									</form>
								</div>
							</div>
							<div class="grid40 start-adding-car">
								<a href="logg-inn.php">Kom i gang <i class="fa fa-chevron-right"></i></a>
							</div>
						</div>
					</div>
				</section>
			<?php endif; ?>
			
			<?php if ( false ) : ?>
				<section>
					<div class="row">
						<button class="btn blue mytrigger">DUMMY - Activate server responses</button>
						<ul id="server-responses"></ul>
					</div>
				</section>
			<?php endif; ?>