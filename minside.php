<?php
/**
 * Min side
 * @package xlbil
 */
require('header.php');
?>
		
		<main>
			<section class="main-section">
				<div class="row">
					<h1>Min side</h1>
					
					<div class="clear">
						<div class="grid50 nopadd-left">
							<p><strong>Navn Navnesen</strong></p>
							<p><strong>(Kundenummer: 7243756)</strong></p>
						</div>
						<div class="grid50 nopadd-right text-right">
							<a class="btn blue" href="selg-bil.php">Legg ut ny bil på auksjon</a>
						</div>
					</div>
					
					<div class="acordion-panels spacer">
						<div class="panel">
							<?php 
								// just some dev shit
								$maxaux = 6;
							?>
							<h3><button type="button" class="expandable-control" aria-controls="mypage-myauctions" aria-expanded="false">Mine auksjoner <span class="blue-text">(<?php echo $maxaux; ?>)</span></button></h3>
							<div id="mypage-myauctions" class="expandable-content" aria-hidden="true">
								<div class="ctabs">
									<ul class="ctabs-tabs">
										<li><a href="#stored" class="active">Lagrede</a></li>
										<li><a href="#running" class="">Pågående <span hide="hide-small">auksjoner</span></a></li>
										<li><a href="#completed" class="">Avsluttede <span hide="hide-small">auksjoner</span></a></li>
									</ul>
									<div class="ctabs-content">
										<div class="ctabs-tab" id="running">
											<?php 
												for ( $i = 0; $i < $maxaux; $i++ ) : ?>
													<div class="myauction-item clear">
														<div class="grid10 nopadd-left">
															<img src="<?php echo zp_im_retreive("assets/img/uploads/mitsubishi_outlander_2014-1.jpg", array(280,160)) ?>" alt="Mitsubishi Outlander" />
														</div>
														<div class="grid20">
															<p><strong>Bil</strong></p>
															<p><span class="make">Mitsubishi</span> <span class="model">Outlander</span>, <span class="engine">1.4 GTI</span>, <span class="reg">SD54123</span></p>
														</div>
														<div class="grid20">
															<p><strong>Auksjonstart</strong></p>
															<p>17. januar 2017, 14.00</p>
														</div>
														<div class="grid20">
															<p><strong>Auksjonslutt</strong></p>
															<p>20. januar 2017, 14.00</p>
														</div>
														<div class="grid10">
															<p><strong>Høyestebud</strong></p>
															<p><?php echo format_kroner( rand(50000, 1500000), true ); ?></p>
														</div>
														
														<div class="grid20 nopadd">
															<a class="btn blue" href="bildetaljer.php" title="Vis auksjon">Vis auksjon</a>
														</div>
													</div>
											<?php endfor; ?>
										</div>
										<div class="ctabs-tab" id="completed">
											<?php 
												for ( $i = 0; $i < $maxaux - 3; $i++ ) : ?>
													<div class="myauction-item clear">
														<div class="grid30 nopadd-left">
															<div class="grid30 nopadd-left">
																<img src="<?php echo zp_im_retreive("assets/img/uploads/mitsubishi_outlander_2014-1.jpg", array(280,160)) ?>" alt="Mitsubishi Outlander" />
															</div>
															<div class="grid70">
																<p><strong>Bil</strong></p>
																<p><span class="make">Mitsubishi</span> <span class="model">Outlander</span>, <span class="engine">1.4 GTI</span>, <span class="reg">SD54123</span></p>
															</div>
														</div>
														<div class="grid20">
															<p><strong>Auksjonslutt</strong></p>
															<p>20. januar 2017, 14.00</p>
														</div>
														<div class="grid20">
															<p><strong>Høyestebud</strong></p>
															<p><?php echo format_kroner( rand(50000, 1500000), true ); ?></p>
														</div>
														<div class="grid20">
															<a class="btn green overlay-trigger" data-overlay='{"id": "#accept-bid", "carid": 968}' href="bildetaljer.php" title="Aksepter bud">Aksepter/Avslå bud</a>
														</div>
														<div class="grid10 nopadd">
															<a class="btn blue" href="bildetaljer.php" title="Vis auksjon">Vis auksjon</a>
														</div>
													</div>
											<?php endfor; ?>
										</div>
										<div class="ctabs-tab" id="stored">
											<?php 
												for ( $i = 0; $i < $maxaux - 2; $i++ ) : ?>
													<div class="myauction-item clear">
														<div class="grid30 nopadd-left">
															<div class="grid30 nopadd-left">
																<img src="<?php echo zp_im_retreive("assets/img/uploads/mitsubishi_outlander_2014-1.jpg", array(280,160)) ?>" alt="Mitsubishi Outlander" />
															</div>
															<div class="grid70">
																<p><strong>Bil</strong></p>
																<p><span class="make">Mitsubishi</span> <span class="model">Outlander</span>, <span class="engine">1.4 GTI</span>, <span class="reg">SD54123</span></p>
															</div>
														</div>
														<div class="grid20">
															&nbsp;
														</div>
														<div class="grid20">
															<a class="btn green overlay-trigger" data-overlay='{"id": "#start-auction", "carid": 428}' href="bildetaljer.php" title="Publiser">Start aukjson</a>
														</div>
														<div class="grid20">
															<a class="btn red overlay-trigger" data-overlay='{"id": "#delete-auction", "carid": 968}' href="#" title="Slett auksjon">Slett auksjon</a>
														</div>
														<div class="grid10 nopadd">
															<a class="btn blue" href="bildetaljer.php" title="Vis auksjon">Vis auksjon</a>
														</div>
													</div>
											<?php endfor; ?>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="panel">
							<?php $maxbids = 8; ?>
							<h3><button type="button" class="expandable-control" aria-controls="mypage-mybids" aria-expanded="false">Mine bud <span class="blue-text">(<?php echo $maxbids; ?>)</span></button></h3>
							<div id="mypage-mybids" class="expandable-content" aria-hidden="true">
								<div class="ctabs">
									<ul class="ctabs-tabs">
										<li><a href="#running-bids" class="">Pågående</a></li>
										<li><a href="#completed-bids" class="">Avsluttede</a></li>
									</ul>
									<div class="ctabs-content">
										<div class="ctabs-tab" id="running-bids">
											<?php 
												for ( $i = 0; $i < $maxbids; $i++ ) : 
													$status = $i % 3 === 0 ? 'highest-bidder' : ''; ?>
													<div class="myauction-item clear <?php echo $status; ?>">
														<div class="grid10 nopadd-left">
															<img src="<?php echo zp_im_retreive("assets/img/uploads/mitsubishi_outlander_2014-1.jpg", array(280,160)) ?>" alt="Mitsubishi Outlander" />
														</div>
														<div class="grid20">
															<p><strong>Bil</strong></p>
															<p><span class="make">Mitsubishi</span> <span class="model">Outlander</span>, <span class="engine">1.4 GTI</span>, <span class="reg">SD54123</span></p>
														</div>
														<div class="grid20">
															<p><strong>Høyestebud</strong></p>
															<p>54 000,-</p>
														</div>
														<div class="grid20">
															<p><strong>Min autobudgrense</strong></p>
															<p>63 000,-</p>
														</div>
														<div class="grid20 your-bid">
															<p><strong>Mitt nåværende bud</strong></p>
															<p>54 000,-</p>

														</div>
														<div class="grid10 nopadd">
															<a class="btn blue" href="bildetaljer.php" title="Vis auksjon">Vis auksjon</a>
														</div>
													</div>
											<?php endfor; ?>
										</div>
										<div class="ctabs-tab" id="completed-bids">
											<?php 
												for ( $i = 0; $i < $maxbids; $i++ ) : 
													$status = $i % 3 === 0 ? 'highest-bidder' : ''; ?>
													<div class="myauction-item clear <?php echo $status; ?>">
														<div class="grid10 nopadd-left">
															<img src="<?php echo zp_im_retreive("assets/img/uploads/mitsubishi_outlander_2014-1.jpg", array(280,160)) ?>" alt="Mitsubishi Outlander" />
														</div>
														<div class="grid20">
															<p><strong>Bil</strong></p>
															<p><span class="make">Mitsubishi</span> <span class="model">Outlander</span>, <span class="engine">1.4 GTI</span>, <span class="reg">SD54123</span></p>
														</div>
														<div class="grid20">
															<p><strong>Høyestebud</strong></p>
															<p>54 000,-</p>
														</div>
														<div class="grid20 your-bid">
															<p><strong>Mitt Bud</strong></p>
															<p>54 000,-</p>
														</div>
														<div class="grid20 nopadd-right">
															<?php if ( $status ) : ?>
																<?php  
																	$status_btns= array(
																		'<a class="btn green" href="mailto:user@example.com" title="Kontakt selger">Godtatt av selger</a>',
																		'<a class="btn red" href="#" title="Avist av selger">Avvist av selger</a>',
																		'<a class="btn grey" href="#" title="Avventer selger">Avventer selger</a>'
																	);
																	$rnd = rand(1, count($status_btns) ) - 1;
																	echo $status_btns[ $rnd ];
																?>
																
															<?php endif; ?>
														</div>
														<div class="grid10 nopadd">
															<a class="btn blue" href="bildetaljer.php" title="Vis auksjon">Vis auksjon</a>
														</div>
													</div>
											<?php endfor; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<?php // Panels for my notifications ?>
						<div class="panel">
							<h3><button type="button" class="expandable-control" aria-controls="mypage-notice" aria-expanded="false">Mine varsler</button></h3>
							<div id="mypage-notice" class="expandable-content" aria-hidden="true">

								<p><?php // Make me editable, please... ?>Her kan du bli varslet når det legges ut nye biler på XLBil.<br>
									I listen nedenfor velger du hvilke merker du ønsker å bli varslet om på e-post. Ønsker du å bli varslet på alle merker så velger du "Alle merker" i listen.</p>

								<form id="user_subscribe_to_notice" method="POST" action="http://eikeland.lan/404.php">
									<ul class="form-box">
										<li>
											<label for="user_subscribed">
												<?php // Make me editable, please... ?>Trykk i feltet for å velge merker å bli varslet om. (du kan også søke etter merke)
												<select id="user_subscribed" name="user_subscribed[]" multiple="multiple">
													<option value="All">Alle merker</option>
													<?php 
														// eks. user subscriptions stored in database
														$my_subscriptions = array('Mercedes-Benz', 'Toyota');

														// maybe another source of brands...
														$brands_list = file_get_contents( 'assets/js/car-brands.json' );
														if ( $brands_list ) {
															$json = json_decode( $brands_list );
															foreach ( $json->brands as $brand ) {
																
																// activate users subscriptions from database
																$selected = in_array( $brand, $my_subscriptions ) ? ' selected' : '';

																echo sprintf( '<option value="%1$s" %2$s>%1$s</option>', $brand, $selected );
															}
														}
													?>
												</select>
											</label>
										</li>
										<li>
											<ul class="form-box by-two">
												<li>
													<button class="btn blue min-200" name="submit_notice_form" type="submit">Lagre varsler</button>
												</li>
												<li class="text-right">
													<button id="user-clear-subscriptions" class="btn red min-200" name="clear" type="button">Stopp alle varsler</button>
												</li>
											</ul>
										</li>
								</form>

							</div>
						</div>
					</div>

					<form id="update-user" class="full-width">
						<fieldset>
							<legend>Personopplysninger</legend>
							<ul class="form-box by-two">
								<li>
									<label for="user-firstname">Fornavn 
										<input type="text" id="user-firstname" name="user-firstname" maxlength="100" required>
									</label>
								</li>

								<li>
									<label for="user-lastname">Etternavn 
										<input type="text" id="user-lastname" name="user-lastname" maxlength="100" required>
									</label>
								</li>

								<li>
									<label>Kjønn</label>
									<ul class="form-box inline">
										<li><label for="user-gender-f" class="ui-radio"><input type="radio" id="user-gender-f" name="user-gender" value="0"><span></span> Kvinne</label></li>
										<li><label for="user-gender-m" class="ui-radio"><input type="radio" id="user-gender-m" name="user-gender" value="0"><span></span> Mann</label></li>
									</ul>
								</li>

								<li>
									<label>Fødselsdato 
										<ul class="form-box by-three">
											<li><input type="number" id="user-birth-date" name="user-birth-date" min="1" max="31" placeholder="Dag" required></li>
											<li><input type="number" id="user-birth-moth" name="user-birth-moth" min="1" max="12" placeholder="Måned" required></li>
											<li><input type="number" id="user-birth-year" name="user-birth-year" min="1917" max="<?php echo date('Y'); ?>" placeholder="År" required></li>
										</ul>
									</label>
								</li>
							</ul>
						</fieldset>

						<fieldset>
							<legend>Firmaopplysninger (valgfritt)</legend>
							<ul class="form-box by-two">
								<li>
									<label for="company-name">Firmanavn 
										<input type="text" id="company-name" name="company-name" maxlength="100">
									</label>
								</li>

								<li>
									<label for="company-person">Kontaktperson 
										<input type="text" id="company-person" name="company-person" maxlength="100">
									</label>
								</li>

								<li>
									<label for="company-id">Organisasjonsnummer
										<input type="text" id="company-id" name="company-id" maxlength="100">
									</label>
								</li>
							</ul>
						</fieldset>


						<fieldset>
							<legend>Adresse og telefon</legend>
							<ul class="form-box by-two">
								<li>
									<label for="user-address">Adresse 
										<input type="text" id="user-address" name="user-address" maxlength="100" required>
									</label>
								</li>

								<li>
									<label for="user-address-2">Adresse 2
										<input type="text" id="user-address-2" name="user-address-2" maxlength="100">
									</label>
								</li>

								<li>
									<label for="user-postnum">Postnummer
										<input type="text" id="user-postnum" class="post-number-input" name="user-postnum" required>
									</label>
								</li>

								<li>
									<label for="user-location">Sted
										<input type="text" id="user-location" class="post-location" name="user-location" maxlength="100" required>
									</label>
								</li>

								<li>
									<label for="user-country">Land
										<input type="text" id="user-country" name="user-country" maxlength="100" value="Norge" disabled>
									</label>
								</li>

								<li>
									<label for="user-telephone">Telefon/mobilnummer
										<input type="tel" id="user-telephone" name="user-telephone" maxlength="100" required>
									</label>
								</li>
							</ul>
						</fieldset>

						<fieldset>
							<legend>Endre passord</legend>
							<ul class="form-box by-two">
								<li>
									<label for="user-password-old">Gammelt passord 
										<input type="text" id="user-password-old" name="user-password-old" maxlength="100" required>
									</label>
								</li>

								<li>
									<label for="user-password">Nytt passord
										<input type="password" id="user-password" name="user-password" maxlength="100" required>
									</label>
								</li>

								<li>
									<label for="user-password-confirm">Bekreft nytt passord
										<input type="password" id="user-password-confirm" name="user-password-confirm" maxlength="100" required>
									</label>
								</li>
						</fieldset>

						<fieldset>
							<ul class="form-box spaced-children">
								
								<li>
									<input type="submit" id="submit-user-form" value="Oppdater informasjon">
								</li>
							</ul>
						</fieldset>
					</form>
						
				</div>
			</section>
		</main>

		<section id="delete-auction" class="overlay">
			
			<div class="viewport">
				
				<div class="row">
				
					<h1>Vil du virkelig slette bilen</h1>

					<p>Sletter du bilen kan den ikke opprettes igjen. Du kan starte en auksjon på samme bil helt gratis om 3 dager.</p>

					<?php  
						// trenger du skjema her eler kan vi bruker linker til auksjonbehandler?
						// Gi beskjed om du trenger det på annan måte?
					?>
					<a class="btn red spaced service-caller" href="service-worker.php?action=delete-car&carid={carid}">Slett bil</a>
					<a class="btn blue overlay-destroy" href="">Avbryt</a>
					
					<a class="close overlay-destroy"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
			
			</div>

		</section>

		<section id="accept-bid" class="overlay">
			
			<div class="viewport">
				
				<div class="row">
				
					<h1>Aksepter eller avslå bud</h1>

					<p>Aksepterer du budet vil du bli kontaktet av bilhandler på neste virkedag.</p>
					
					<?php  
						// trenger du skjema her eler kan vi bruker linker til auksjonbehandler?
						// Gi beskjed om du trenger det på annan måte?
					?>
					<a class="btn green spaced service-caller" href="service-worker.php?action=accept-bid&carid={carid}">Aksepter</a>
					<a class="btn red spaced service-caller" href="service-worker.php?action=deny-bid&carid={carid}">Avslå</a>
					<a class="btn blue overlay-destroy" href="">Avbryt</a>
					
					<a class="close overlay-destroy"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
			
			</div>

		</section>
		<section id="start-auction" class="overlay">
			
			<div class="viewport">
				
				<div class="row">
				
					<h1>Start auksjon</h1>

					<p>Jeg vil starte 3 dagers auksjon på denne bilen nå. auksjonen avsluttes <?php echo date('l d. F Y \k\l H:i:s', time() + 60 * 60 * 24 * 3); ?>.</p>
					
					<?php  
						// trenger du skjema her eler kan vi bruker linker til auksjonbehandler?
						// Gi beskjed om du trenger det på annan måte?
					?>
					<a class="btn green spaced service-caller" href="service-worker.php?action=accept-bid&carid={carid}">Start auksjon</a>
					<a class="btn blue overlay-destroy" href="">Avbryt</a>
					
					<a class="close overlay-destroy"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
			
			</div>

		</section>
<?php require('footer.php'); ?>
