<?php
/**
 * Login page
 * @package xlbil
 */
require('header.php');
?>
		
		<main>
			<section class="main-section">
				<div class="row">
					<h1>Logg inn</h1>
					<p>Ikke registrert? <a href="registrer.php">Registrer deg her.</a></p>

					<div class="form-wrapper">
						<div class="form-switchers">
							<button type="button" id="activate-form-user" class="btn active" data-activate="#login-user">Selger (privat)</buttton>
							<button type="button" id="activate-form-buyer" class="btn grey" data-activate="#login-buyer">Kjøper (bilhandler)</buttton>
						</div>

						<div class="forms">
							<form id="login-user" class="active" method="post" action="">
								<ul class="form-box">
									<li>
										<label for="login-user-username">Brukernavn (e-postadresse)
											<input type="email" id="login-user-username" name="login-user-username" maxlength="100" autofocus required/>
										</label>
									</li>
									<li>
										<label for="login-user-password">Passord
											<input type="password" id="login-user-password" name="login-user-password" maxlength="100" required/>
										</label>
									</li>
									<li>
										<ul class="form-box by-three v-center block-on-small">
											<li>
												<button type="submit" name="submit-login-user" value="login" class="btn blue">Logg inn</button>
											</li>
											<li>
												<button type="submit" name="forgot-password-user" value="forgot-password" class="btn blue">Glemt passord?</button>
											</li>
											<li>
												<label for="login-user-remember" class="ui-check">Husk meg
													<span>
														<input type="checkbox" id="login-user-remember" name="login-user-remember">
														<span></span>
													</span>
												</label>
											</li>
										</ul>
									</li>
									<li><p>Hvis du har glemt passordet ditt, skriv inn ditt brukernavn (epostadresse) og klikk "send passord"</p></li>
								</ul>
							</form>

							<form id="login-buyer" method="post" action="">
								<ul class="form-box">
									<li>
										<label for="login-buyer-username">Forhandler (e-postadresse)
											<input type="email" id="login-buyer-username" name="login-buyer-username" maxlength="100" autofocus required/>
										</label>
									</li>
									<li>
										<label for="login-buyer-password">Passord
											<input type="password" id="login-buyer-password" name="login-buyer-password" maxlength="100" required/>
										</label>
									</li>
									<li>
										<ul class="form-box by-three v-center block-on-small">
											<li>
												<button type="submit" name="submit-login-company" value="login" class="btn blue min-200">Logg inn</button>
											</li>
											<li>
												<button type="submit" name="forgot-password-company" value="forgot-password" class="btn blue">Glemt passord?</button>
											</li>
											<li>
												<label for="login-buyrer-remember" class="ui-check">Husk meg
													<span>
														<input type="checkbox" id="login-buyrer-remember" name="login-buyrer-remember">
														<span></span>
													</span>
												</label>
											</li>
										</ul>
										<li><p>Hvis du har glemt passordet ditt, skriv inn ditt brukernavn (epostadresse) og klikk "send passord"</p></li>
									</li>
								</ul>
							</form>
						</div>
					</div>
				</div>
			</section>
		</main>

<?php require('footer.php'); ?>
