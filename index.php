<?php
/**
 * Main index file
 * @package xl-bil
 */
require('header.php');
?>
		<main>

			<section class="main-section">
				<div class="row">
					<div class="clear">
						<div class="grid75 nopadd">
							
							<h1 class="grid product-list-title">Utvalgte pågående auksjoner</h1>
							
							<span class="seperator"></span>
							
							<ul class="product-list">
								<?php for ( $i=0; $i < 6; $i++) : ?>
											<?php 
												// Dummy id for testing schema
												$id = "product-item-".rand(1000, 9999); 
											?>
											<li class="grid33 grid-medium-2 product-item-wrapper">
												<?php include('part-carpreview.php'); ?>
											</li>
											
											<?php if ( $i === 1 ) { ?>
											<?php // show add for mobile as the third article ?>
												<li class="grid33 grid-medium-2 product-item-wrapper ad ad-mobile">
													<article>
														<a class="ad-link" href="https://track.adform.net/C/?bn=25444721">
															<img class="ad-image" src="assets/img/naf-test_660x377-1.jpg" alt="Annonse">
															<img src="https://track.adform.net/adfserve/?bn=25444721;1x1inv=1;srctype=3;ord=[timestamp]" border="0" width="1" height="1"/>
														</a>
													</article>
												</li>
											<?php } ?>
											
								<?php endfor; ?>
							</ul>

							<div class="grid spacer">
								<a href="alle-auksjoner.php" class="btn blue block" title="Alle auksjoner">Se alle auksjoner</a>
							</div>

						</div>

						<aside class="grid25 promo-boxes">
							<article class="promo-box">
								<h2>Selg din bil på auksjon!</h2>
								<p>Det er bare registrerte norske bilhandlere som kan by på bilen din. XL Bil har allerede 300 registrerte bilhandlere.</p> 
								<p>I de 72 timene auksjonen varer, vil du motta en e-post hver gang en bilforhandler byr på bilen din. Du er dermed alltid oppdatert på hvordan det går med bilen din på auksjonen.</p>

							</article>
							<article class="promo-box promo-cta">
								<h3>Sikker handel!</h3>
								<p>På XLBil averterer du overfor proffesjonell norske bilhandlere som tilbyr trygg handel og ordnede former!</p>
							</article>

							<article class="promo-box">
								<h2>Ja, det er gratis!</h2>
								<p>Det koster ingen ting for deg som privatperson å legge ut bilen din på auksjon til forhandlere.</p>
								<p>Les mer om våre <a href="om-oss.php#about-terms">vilkår</a></p>
							</article>

							<article class="ad ad-desktop">
								<a class="ad-link" href="https://track.adform.net/C/?bn=25444721">
									<img class="ad-image" src="assets/img/naf-test_660x377-1.jpg" alt="Annonse">
									<img src="https://track.adform.net/adfserve/?bn=25444721;1x1inv=1;srctype=3;ord=[timestamp]" border="0" width="1" height="1"/>
								</a>
							</article>
						</aside>
					</div>
				</div>
			</section>

			<section>
				<div class="row">
					<h1 class="grid product-list-title">Avsluttede auksjoner</h1>
					
					<span class="seperator"></span>

					<ul class="product-list">
						<?php for ( $i=0; $i < 10; $i++) : ?>
							
							<li class="grid20 grid-medium-3 grid-small-2 product-item-wrapper">
								<?php 
									$include_type = "sold-cars";
									include('part-carpreview.php');
								?>
							</li>
						<?php endfor; ?>
					</ul>
				</div>
			</section>
		</main>
		
<?php require('footer.php'); ?>