<?php 
	// Dummy id for testing schema
	$id = "product-item-".rand(1000, 9999); 
	$cost = rand(50000, 1500000);
?>
<article id="<?php echo $id; ?>" class="product-item" itemscope itemtype="http://schema.org/Car">
	<a href="bildetaljer.php" class="item-prew" itemprop="url">
		<?php  
			$img = "assets/img/uploads/mitsubishi_outlander_2014-1.jpg";
			$img_preview = zp_im_retreive( $img, array(280,160));
			$img_mobile = zp_im_retreive( $img, array(660,377));			
		?>
		<img src="<?php echo $img_preview; ?>" srcset="<?php echo $img_preview ?> 280w, <?php echo $img_mobile; ?> 660w" sizes="(min-width: 991px) 280px, 660px" alt="Mitsubishi Outlander 2014" itemprop="image">
		<span class="item-timer">
			<span>3 <span>t</span></span>
			<span>54 <span>min</span></span>
		</span>

		<span class="condition-verified">Tilstandsrapport</span>
	</a>
	
	<header class="item-header">
		<?php  
			$result_inputdata = array(
				'merkeNavn' => "Mitsubishi",
				'modellbetegnelse' => "Outlander 1.4 GTI",
				'motorytelse' => 92,00,
				'regAAr' => "2004",
				'drivstoff' => "Bensin",
				'kjennemerke' => "ST91987",
				'girkasse' => "Manuell",
			);
			$result_userinput = array(
				'kilometerstand' => 142350,
				'kommune' => 'Stord'
			);
			$title = array($result_inputdata['merkeNavn'], $result_inputdata['modellbetegnelse'], $result_inputdata['kjennemerke']);
			$specs = array( $result_inputdata['regAAr'], $result_inputdata['drivstoff'], round(($result_inputdata['motorytelse'] * 1.36)) . " hk", $result_inputdata['girkasse'], $result_userinput['kilometerstand'] . " km", '<a href="https://maps.google.com/?q=' . $result_userinput['kommune'] . '" target="_blank">' . $result_userinput['kommune'] . '</a>');
		?>
		<h2 itemprop="name">
			<a href="bildetaljer.php"><?php echo implode( " ", $title ); ?></a>
		</h2>
		<span class="item-spec" itemprop="description"><?php echo implode( ' &#47; ', $specs ); ?></span>
	</header>

	<footer class="item-footer" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<span itemprop="itemcondition" content="used"></span>
		<div class="item-price" itemprop="priceSpecification" itemscope itemtype="http://schema.org/UnitPriceSpecification">
			<span class="hidden" itemprop="priceCurrency">NOK</span>
			<span class="hidden" itemprop="price"><?php echo format_kroner($cost, false, true); ?></span>
			<?php  
				$your_bid_text = "Du kan by:";

				if (!empty($include_type) && $include_type === 'sold-cars' ) {
					$your_bid_text = "Solgt for:";
				}
			?>
			<span class="info-next-bid"><?php echo $your_bid_text; ?></span>
			<a href="bildetaljer.php"><?php echo format_kroner($cost, true); ?></a>
		</div>
	</footer>
	
</article>