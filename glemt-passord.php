<?php
/**
 * Main index file fior xl-bil
 */
require('header.php');
?>
		<main>
			<section class="main-section">
				<div class="row">
					<h1>Glemt passord</h1>
					<p>Skriv inn din e-postadresse og be om nytt passord. Du vil da få en e-post med mulighet til å generere et nytt passord.</p>
					
					<form id="forgotten-passwd" method="post" action="">
						<ul class="form-box">
							<li>
								<label for="forgotten-passwd-username">Brukernavn (e-postadresse)
									<input type="email" id="forgotten-passwd-username" name="forgotten-passwd-username" maxlength="100" autofocus required/>
								</label>
							</li>
							<li>
								<button type="submit" class="btn blue">Be om nytt passord</button>
							</li>
						</ul>
					</form>
				</div>
			</section>
		</main>
<?php require('footer.php'); ?>
