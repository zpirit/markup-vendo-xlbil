<?php
/**
 * All auctions page
 * @package xlbil
 */
require('header.php');
?>
		<main>
			<section class="main-section">
				<div class="row">
					<h1 class="grid">Alle auksjoner</h1>

					<span class="grid total-hits">Viser 16 av 240 treff</span>
					<span class="seperator"></span>
					<div class="grid filters clear">
						<span class="filter-item filter-county">
							<span class="filter-selected">Fylke</span>
							<input type="text" id="choose-county" name="choose-county">
							<ul>
								<li><a href="#" data-value="standard">Fylke</a></li>
								<li><a href="#" data-value="akershus">Akershus</a></li>
								<li><a href="#" data-value="aust-agder">Aust-Agder</a></li>
								<li><a href="#" data-value="buskerud">Buskerud</a></li>
								<li><a href="#" data-value="finnmark">Finnmark</a></li>
								<li><a href="#" data-value="hedmark">Hedmark</a></li>
								<li><a href="#" data-value="hordaland">Hordaland</a></li>
								<li><a href="#" data-value="oslo">Oslo</a></li>
								<li><a href="#" data-value="more-og-romsdal">Møre og Romsdal</a></li>
								<li><a href="#" data-value="nord-trondelag">Nord-Trøndelag</a></li>
								<li><a href="#" data-value="nordland">Nordland</a></li>
								<li><a href="#" data-value="oppland">Oppland</a></li>
								<li><a href="#" data-value="rogaland">Rogaland</a></li>
								<li><a href="#" data-value="sogn-og-fjordane">Sogn og Fjordane</a></li>
								<li><a href="#" data-value="sor-trondelag">Sør-Trøndelag</a></li>
								<li><a href="#" data-value="telemark">Telemark</a></li>
								<li><a href="#" data-value="troms">Troms</a></li>
								<li><a href="#" data-value="vest-agder">Vest-Agder</a></li>
								<li><a href="#" data-value="vestfold">Vestfold</a></li>
								<li><a href="#" data-value="ostfold">Østfold</a></li>
								<li><a href="#" data-value="svalbard">Svalbard</a></li>
							</ul>
						</span>

						<span class="filter-item filter-body">
							<span class="filter-selected">Karosseri</span>
							<input type="text" id="choose-body" name="choose-body">
							<ul>
								<li><a href="#" data-value="standard">Karosseri</a></li>
								<li><a href="#" data-value="cabriolet">Cabriolet</a></li>
								<li><a href="#" data-value="coupe">Coupe</a></li>
								<li><a href="#" data-value="flerbruksbil">Flerbruksbil</a></li>
								<li><a href="#" data-value="kasse">Kasse</a></li>
								<li><a href="#" data-value="kombi-3-dors">Kombi 3-dørs</a></li>
								<li><a href="#" data-value="kombi-5-dors">Kombi 5-dørs</a></li>
								<li><a href="#" data-value="pickup">Pickup</a></li>
								<li><a href="#" data-value="suv-offroad">SUV/Offroad</a></li>
								<li><a href="#" data-value="sedan">Sedan</a></li>
								<li><a href="#" data-value="stasjonsvogn">Stasjonsvogn</a></li>
								<li><a href="#" data-value="annet">Annet</a></li>
							</ul>
						</span>
						
						<span class="filter-item filter-brand">
							<span class="filter-selected">Merke</span>
							<input type="text" id="choose-brand" name="choose-brand">
							<ul>
								<li><a href="#" data-value="standard">Merke</a></li>
								<?php //"loop over cars brands on this page" ?>
								<li><a href="#" data-value="mitsubishi">Mitsubishi</a></li>
							</ul>
						</span>

						<span class="filter-item filter-sort">
							<span class="filter-selected">Velg sortering</span>
							<input type="text" id="sort-items" name="sort-items">
							<ul>
								<li><a href="#" data-value="standard">Velg sortering</a></li>
								<li><a href="#" data-value="sort-finish-first">Avsluttes først</a></li>
								<li><a href="#" data-value="sort-finish-last">Avsluttes sist</a></li>
								<li><a href="#" data-value="sort-bid-lowest">Laveste bud</a></li>
								<li><a href="#" data-value="sort-bid-highest">Høyeste bud</a></li>
								<li><a href="#" data-value="sort-latest">Nyeste auksjoner</a></li>
							</ul>
						</span>
					</div>
							
					<ul class="product-list sortable">
						<?php for ( $i=0; $i < 16; $i++) : ?>
							<?php 
								// Dummy id for testing schema
								// Remove me before prod.
								$id = "product-item-".rand(1000, 9999); 
							?>
							<li class="grid25 grid-medium-2 product-item-wrapper">
								<?php include('part-carpreview.php'); ?>
							</li>
						<?php endfor; ?>
					</ul>
					
					<div class="grid">
						<?php //eventual use pagination? ?>
						<a href="#" class="load-more-items btn blue block" title="Last inn flere auksjoner">Last inn flere</a>
					</div>
				</div>
			</section>
		</main>
<?php require('footer.php'); ?>
