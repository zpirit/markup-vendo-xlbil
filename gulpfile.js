// Dependencies and proccessors
var gulp = require('gulp'),
	postcss = require('gulp-postcss'),
	cssnano = require('cssnano'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	autoprefixer = require('autoprefixer'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	pump = require('pump'),
	browserSync = require('browser-sync').create();

gulp.task('style', function(cb){
	pump([
			gulp.src([
				'assets/css/style.scss'
			]),
			sourcemaps.init(),
			sass({
				outputStyle: 'expanded'
			}),
			postcss([
				autoprefixer({ browsers: ['last 3 versions'] }),
				cssnano()
			]),
			sourcemaps.write(),
			gulp.dest('./')
		],
		cb
	);
});

gulp.task('script', function(cb) {
	pump([
			gulp.src([
				'assets/js/author.js', 
			]),
			concat('author.min.js'),
			uglify(),
			gulp.dest('./assets/js/')
		],
		cb
	);
});

gulp.task('vendor', function(cb) {
	pump([
			gulp.src('assets/js/vendor-src/*.js'),
			concat('vendor.js'),
			uglify(),
			gulp.dest('./assets/js/')
		],
		cb
	);
});

// Static Server + watching scss files
gulp.task('serve', ['style'], function() {

    browserSync.init({
        proxy: "localhost/xlbil/"
    });

    gulp.watch('assets/css/*.scss', ['style']);
	gulp.watch('assets/js/author.js', ['script']);
    gulp.watch('style.css').on('change', browserSync.reload);
});

gulp.task('watch', function() {
	gulp.watch('assets/css/*.scss', ['style']);
	gulp.watch('assets/js/author.js', ['script']);
});

gulp.task('default', ['serve']);
