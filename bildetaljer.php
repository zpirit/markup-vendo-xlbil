<?php
/**
 * Main index file fior xl-bil
 */
require('header.php'); ?>
		<main>
			<section class="main-section">
				<div class="row">
					<article class="single-car">
						<div class="clear">
							<div class="grid50">
								<div class="car-detail-slider">
									<span class="condition-verified">Tilstandsrapport</span>
									<div id="slider" class="flexslider">
										<ul class="slides">
											<?php  
												$slides = array(
													"assets/img/uploads/mitsubishi_outlander_2014-1.jpg", 
													"assets/img/uploads/mitsubishi_outlander_2014-2.jpg", 
													"assets/img/uploads/mitsubishi_outlander_2014-3.jpg", 
													"assets/img/uploads/mitsubishi_outlander_2014-4.jpg", 
													"assets/img/uploads/mitsubishi_outlander_2014-5.jpg", 
													"assets/img/uploads/mitsubishi_outlander_2014-6.jpg",
													"assets/img/uploads/mitsubishi_outlander_2014-7.jpg",
													"assets/img/uploads/mitsubishi_outlander_2014-8.jpg",
													"assets/img/uploads/mitsubishi_outlander_2014-9.jpg"
												);
												foreach ( $slides as $slide ) {
													// Creates image size, see function.php
													$img = zp_im_retreive($slide, array(660, 377));
													echo sprintf( '<li><a data-fancybox="gallery" href="%1$s"><img src="%1$s" alt="Mitsubishi Outlander SUV 4WD"></a></li>', $img );
												}
											?>
										</ul>
									</div>

									<div id="carousel" class="flexslider">
										<ul class="slides">
											<?php  
												foreach ( $slides as $thumb ) {
													// Creates image size, see function.php
													$img = zp_im_retreive($thumb, array(120, 69));
													echo sprintf( '<li><img src="%s" alt="Mitsubishi Outlander SUV 4WD"></li>', $img );
												}
											?>
										</ul>
									</div>
								</div>
							</div>
							
							<div class="grid50">
								<header>
									<?php  
										$result_inputdata = array(
											'merkeNavn' => "Mitsubishi",
											'modellbetegnelse' => "Outlander 1.4 GTI",
											'motorytelse' => 92,00,
											'regAAr' => "2004",
											'drivstoff' => "Bensin",
											'kjennemerke' => "ST91987",
											'girkasse' => "Manuell",
										);
										$result_userinput = array(
											'kilometerstand' => 142350,
											'kommune' => 'Stord'
										);
										$title = array($result_inputdata['merkeNavn'], $result_inputdata['modellbetegnelse'], $result_inputdata['kjennemerke']);
										$specs = array( $result_inputdata['regAAr'], $result_inputdata['drivstoff'], round(($result_inputdata['motorytelse'] * 1.36)) . " hk", $result_inputdata['girkasse'], $result_userinput['kilometerstand'] . " km", '<a href="https://maps.google.com/?q=' . $result_userinput['kommune'] . '" target="_blank">' . $result_userinput['kommune'] . '</a>');
									?>
									<h1><?php echo implode( " ", $title ); ?></h1>
									<span class="spec"><?php echo implode( ' &#47; ', $specs ); ?></span>
								</header>
								<ul class="bid-specs">
									<li>
										<p>Høyeste bud: <span class="gridright">Ingen bud avgitt</span></p>
									</li>
									<li>
										<p>Budøkning: <span class="gridright">1000,-</span></p>
									</li>
									<li>
										<?php 
											$allowed_to_bid = TRUE;
											$disabled = $allowed_to_bid ? "" : "disabled";
											$add_bid_btn = $allowed_to_bid ? "Legg inn bud" : "Bare forhandler kan by";
										?>
										<form id="pre-bid" action="">
											<label for="user-bid" class="left-inline">Ditt bud: 
												<input type="text" maxlength="15" id="user-bid" name="user-bid" <?php echo $disabled; ?>>
											</label>

											<label for="user-autobid" class="left-inline">Ditt autobud: 
												<input type="text" maxlength="15" id="user-autobid" name="user-autobid" <?php echo $disabled; ?>>
											</label>
											<div class="clear">
												<button type="submit" value="submit-bid" class="btn blue right-button" <?php echo $disabled; ?>><?php echo $add_bid_btn; ?></button>
											</div>
											<p class="waver">Du vil bekrefte budet i neste steg. 
																<a href="om-oss.php#about-faq-panel" class="tooltip-parent">Les mer om autobud <i class="fa fa-info-circle" aria-hidden="true"></i>
																	<span id="about-autobid-tooltip" class="tooltip">
																		<span class="tooltip-content">
																			<span class="tooltip-title">Autobud</span>
																			Autobud funksjonaliteten registrerer det høyeste beløpet du ønsker å gi for kjøretøyet. Ved bruk av autobud vil systemet automatisk plassere bud for deg med minste budøkning på kr. 1000.- frem til ditt autobud er nådd. Du kan endre ditt autobud ved å legge inn nytt ønsket beløp i feltet for autobud. Autobud må minimum settes til høyeste bud pluss minste tillatte budøkning. Et autobud kan ikke slettes.
																			<span class="tooltip-link" data-target="om-oss.php#about-faq-panel">Mer om autobud</span>
																		</span>
																	</span>
																</a></p>
										</form>
									</li>
									<li>
										<div class="with-timer">
											Gjenstående tid: 
											<?php 
												// Just for testing
												// data-endtime formater:
												// YYYY/MM/DD
												// MM/DD/YYYY
												// YYYY/MM/DD hh:mm:ss
												// MM/DD/YYYY hh:mm:ss
												$date_from = time() - 86400;
												$date_to = time() + 60;
												// $date_to = time() + (10);
											?>
											<span class="gridright remaining-time" data-endtime="<?php echo date( 'Y/m/d H:i:s', $date_to); ?>">
												<span class="block">
													<span id="remaining-time-days" class="digits"></span>
													Dager
												</span>
												<span class="block">
													<span id="remaining-time-hours" class="digits"></span>
													Timer
												</span>
												<span class="block">
													<span id="remaining-time-minutes" class="digits"></span>
													Minutt
												</span>
												<span class="block">
													<span id="remaining-time-seconds" class="digits"></span>
													Sekund
												</span>
											</span>
										</div>
									</li>
									<li>
										<p class="start-end-time">Startet: <?php echo date( 'd. F Y, H:i', $date_from ); ?> <span class="gridright">Avsluttes: <?php echo date( 'd. F Y, H:i:s', $date_to ); ?></span></p>
									</li>
								</ul>
							</div>
						</div>

						<div class="single-car-content clear">
							<div class="grid50">
								<section>
									<h2 class="section-title"><button type="button" class="btn expandable-control" aria-controls="section-expand-1" aria-expanded="true">Produktinformasjon</button></h2>
									<div id="section-expand-1" class="expandable-content expanded" aria-hidden="false">
										<ul class="car-docs">
											<li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Tilstandsrapport / NAF test</a></li>
											<li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Tekniske data.pdf</a></li>
											<li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Servicehefte.pdf</a></li>
										</ul>
										<p>Som «NY» Opel Corsa selges. Kjøp ny hos oss i 2016. Det medfølger sommer og vinterdekk på alufelg (pigg på vinter) Bilen har akkurat hatt service. Sprek motor som grå meget stille. Nesten ny Opel Corsa Premium med automatgir. Topputstyrt variant med blant annet DAB-radio, ryggekamera, parksensorer foran og bak, varme i ratt, regnsensor, med mer.</p>
									</div>
								</section>

								<section>
									<h2 class="section-title"><button type="button" class="btn expandable-control" aria-controls="section-expand-2" aria-expanded="true">Utstyr</button></h2>
									<div id="section-expand-2" class="expandable-content expanded" aria-hidden="false">
										<ul class="clear inline-50">
											<li>ABS bremser</li>
											<li>Aluminiumsfelger</li>
											<li>Antispinn</li>
											<li>Automatisk klimaanlegg</li>
											<li>Beltevarsler</li>
											<li>Delte bakseter</li>
											<li>Delvis skinnseter</li>
											<li>Elektriske speil</li>
											<li>Elektriske vindusheiser</li>
											<li>Elektronisk bremsekraftfordeler</li>
											<li>Høydejusterbart ratt</li>
											<li>Isofix barnesetefesting</li>
											<li>Kjørecomputer</li>
											<li>Kollisjonsputer</li>
											<li>Lakkerte støtfangere</li>
											<li>Lakkerte utvendige speil</li>
											<li>Lengdejusterbart ratt</li>
											<li>Multifunksjonsratt</li>
											<li>Nedfellbare bakseter</li>
											<li>Nødbremseforsterker</li>
											<li>Oppvarmet frontrute</li>
											<li>Oppvarmet speil</li>
											<li>Parkering avstandsføler bak</li>
											<li>Parkering avstandsføler foran</li>
											<li>Pollenfilter</li>
											<li>Radio/cd spiller</li>
											<li>Regnsensor</li>
											<li>Sentrallås</li>
											<li>Stabilitetssystem antiskrens</li>
											<li>Startsperre</li>
											<li>Tonede ruter</li>
											<li>Tåkelys foran</li>
											<li>Varme i seter</li>
											<li>Xenonlys</li>
										</ul>
									</div>
								</section>
							</div>
							<div class="grid50">

								<section>
									<h2 class="section-title"><button type="button" class="btn expandable-control" aria-controls="section-expand-5" aria-expanded="true">Viktig salgsinformasjon</button></h2>
									<div id="section-expand-5" class="expandable-content expanded" aria-hidden="false">
										<ul>
											<li>Selger: <span class="gridright">Roger Rabitt</span></li>
											<li>Lokasjon: <span class="gridright">Stord, Norge</span></li>
											<li>Kilometerstand: <span class="gridright">54 362 km</span></li>
											<li>Antall nøkler: <span class="gridright">3</span></li>
											<li>Er alle servicer fulgt? <span class="gridright">Ja</span></li>
											<li>Er registerreim byttet? <span class="gridright">Nei</span></li>
											<li>Er bilen importert? <span class="gridright">Nei</span></li>
											<li>Har bilen vert skadet? <span class="gridright">Ja</span></li>
											<li>Er det sommer og vinterhjul på felg til bilen? <span class="gridright">Ja</span></li>
											<li>Er det riper eller sprekker i frontruta? <span class="gridright">Nei</span></li>
											<li>Har bilen bulker eller riper utvendig? <span class="gridright">Nei</span></li>
											<li>Har interiøret synlig slitasje eller defekter? <span class="gridright">Ja</span></li>
											<li>Er det dugg eller fukt i lykter? <span class="gridright">Nei</span></li>

										</ul>

										<p><strong>Generelle vilkår</strong></p>

										<ul class="standard">
											<li>Alle bud inngitt på auksjonen er juridisk bindende.</li>
											<li>Selger har rett til å avslå ethvert vinnerbud.</li>
											<li>Selger har 24 timer på seg til å avgjøre om vinnerbudet er godtatt eller avslått.</li>
											<li>Det er ikke tillatt med budsamarbeid.</li>
											<li>Har du avgitt feil sum på budet ditt, kontakt <a href="mailto:kundeservice@xlbil.no">kundeservice@xlbil.no</a> snarest.</li>
											<li>Frakt og avlevering avtalest mellom selger og kjøper.</li>
										</ul>
									</div>
								</section>

								<section>
									<h2 class="section-title"><button type="button" class="btn expandable-control" aria-controls="section-expand-6" aria-expanded="true">Budhistorikk</button></h2>
									<div id="section-expand-6" class="expandable-content expanded" aria-hidden="false">
									
									<table class="bid-history">
										<colgroup span="3">
										<thead>
											<tr>
												<th>Forhandler</th>
												<th>Buddato</th>
												<th>Bud</th>
											</tr>
										</thead>
										<tbody>
											<?php  
												$bidders =  array(
													array('Yosemity Sam',     true,  date('d/m/Y H:i:s', time() - 20), 154000),
													array('Oddvar Hårde',     false, date('d/m/Y H:i:s', time() - 23), 152000),
													array('John Einar Olsen', true,  date('d/m/Y H:i:s', time() - 146), 150000),
													array('Kjetil Belbo',     false, date('d/m/Y H:i:s', time() - 141), 148000),
													array('Daniel Kesam',     false, date('d/m/Y H:i:s', time() - 236), 146000),
													array('Espen Kvalheim',   true,  date('d/m/Y H:i:s', time() - 87230), 130000),
													array('Øyvind Eikeland',  false, date('d/m/Y H:i:s', time() - 87235), 2000)
												);

												foreach ( $bidders as $bidder ) {
													$class = $bidder[1] ? 'bid_max' : '';

													echo sprintf( '<tr class="%s"><td>%s</td><td>%s</td><td>%s</td></tr>', $class, $bidder[0], $bidder[2], format_kroner($bidder[3], true) );
												}
											?>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="3"><i class="fa fa-refresh" aria-hidden="true"></i> = Autobud</td>
											</tr>
										</tfoot>
									</table>
								</section>

								<section>
									<h2 class="section-title"><button type="button" class="btn expandable-control" aria-controls="section-expand-3" aria-expanded="true">Vognkortdata</button></h2>
									<div id="section-expand-3" class="expandable-content expanded" aria-hidden="false">
										<?php 
											// tekst for felter fra inputdata 
											$output_data = array(
												"akseltrykkBak" => "", 
												"akseltrykkForan" => "", 
												"antAksler" => "Antall aksler", 
												"antAkslerDrift" => "Antall aksler med drift", 
												"bredde" => "Bredde", 
												"bruktimport" => "", 
												"co2utslipp" => "CO2-utslipp", 
												"drivendeHjul" => "", 
												"drivstoff" => "", 
												"egenvekt" => "Egenvekt", 
												"euHovednummer" => "", 
												"euronorm" => "", 
												"EUvariant" => "", 
												"EUversjon" => "", 
												"fabPartFilter" => "Partikkelfilter", 
												"farge" => "Farge", 
												"fjernet" => "", 
												"girkasse" => "", 
												"gyldigFraUnr" => "", 
												"identitetAnm" => "", 
												"kjennemerke" => "Registreringsnummer", 
												"kjoretoygruppe" => "", 
												"lengde" => "Lengde", 
												"lngdTilhKobl" => "", 
												"maksSporvBak" => "", 
												"maksSporvForan" => "", 
												"maxBelTilhFeste" => "", 
												"maxTaklast" => "", 
												"merkekode" => "", 
												"merkeNavn" => "Merke", 
												"minHastBak" => "", 
												"minHastForan" => "", 
												"minInnpressBak" => "", 
												"minInnpressForan" => "", 
												"minLIbak" => "", 
												"minLIforan" => "", 
												"modellbetegnelse" => "Modell", 
												"motormerking" => "", 
												"motorytelse" => "Motorytelse", 
												"nestePKK" => "Neste EU-kontroll", 
												"omdreininger" => "", 
												"rammeKarosseri" => "", 
												"regAAr" => "Registreringsår", 
												"regFgang" => "", 
												"regFoerstegNorge" => "Registrert første gang i Norge", 
												"sisteRegDato" => "Registrert første gang på eier", 
												"sistPKKgodkj" => "Sist EU-godkjent", 
												"sitteplasser" => "Antall seter", 
												"slagvolum" => "Slagvolum", 
												"standStoy" => "", 
												"stdDekkBak" => "Dekk bak (standard)", 
												"stdDekkForan" => "Dekk foran (standard)", 
												"stdFelgBak" => "Felg bak (standard)", 
												"stdFelgForan" => "Felg foran (standard)", 
												"tilhVktMbrems" => "", 
												"tilhVktUbrems" => "", 
												"totalvekt" => "Tillatt totalvekt", 
												"typebetegnelse" => "", 
												"typegodkjenningsnr" => "", 
												"understellsnummer" => "Understellsnummer", 
												"vognkortAnm" => "", 
												"vogntogvekt" => "", 
												"ytelseBenevn" => ""
											);

											// resultat fra input data
											$input_data = array(
												"akseltrykkBak" => "1035", 
												"akseltrykkForan" => "1010", 
												"antAksler" => "2", 
												"antAkslerDrift" => "2", 
												"bredde" => "174", 
												"bruktimport" => "0", 
												"co2utslipp" => "0000", 
												"drivendeHjul" => "--", 
												"drivstoff" => "1", 
												"egenvekt" => "1390", 
												"euHovednummer" => "--", 
												"euronorm" => "", 
												"EUvariant" => "", 
												"EUversjon" => "", 
												"fabPartFilter" => "0", 
												"farge" => "SØLV", 
												"fjernet" => "0", 
												"girkasse" => "--", 
												"gyldigFraUnr" => "--", 
												"identitetAnm" => "--", 
												"kjennemerke" => "ST91987", 
												"kjoretoygruppe" => "101", 
												"lengde" => "445", 
												"lngdTilhKobl" => "", 
												"maksSporvBak" => "1485", 
												"maksSporvForan" => "1495", 
												"maxBelTilhFeste" => "", 
												"maxTaklast" => "", 
												"merkekode" => "5260", 
												"merkeNavn" => "SUBARU", 
												"minHastBak" => "", 
												"minHastForan" => "S", 
												"minInnpressBak" => "48", 
												"minInnpressForan" => "48", 
												"minLIbak" => "86", 
												"minLIforan" => "86", 
												"modellbetegnelse" => "FORESTER 2.0X AWD", 
												"motormerking" => "--", 
												"motorytelse" => "92,00", 
												"nestePKK" => "2016-11-30", 
												"omdreininger" => "--", 
												"rammeKarosseri" => "--", 
												"regAAr" => "2004", 
												"regFgang" => "0630", 
												"regFoerstegNorge" => "2004-06-30", 
												"sisteRegDato" => "2011-06-24", 
												"sistPKKgodkj" => "2014-11-11", 
												"sitteplasser" => "5", 
												"slagvolum" => "1994", 
												"standStoy" => "--", 
												"stdDekkBak" => "205/70R15", 
												"stdDekkForan" => "205/70R15", 
												"stdFelgBak" => "15X6J", 
												"stdFelgForan" => "15X6J", 
												"tilhVktMbrems" => "", 
												"tilhVktUbrems" => "", 
												"totalvekt" => "1880", 
												"typebetegnelse" => "JF1SG", 
												"typegodkjenningsnr" => "--", 
												"understellsnummer" => "JF1SG5LR54G032339", 
												"vognkortAnm" => "--", 
												"vogntogvekt" => "", 
												"ytelseBenevn" => ''
											);
										
											$vognkort_display = array(
												array("EU-kontroll", array("sistPKKgodkj", "nestePKK")),
												array("Registreringsdata", array("kjennemerke", "merkeNavn", "modellbetegnelse", "regAAr", "sitteplasser", "understellsnummer", "regFoerstegNorge", "sisteRegDato")),
												array("Miljø", array("co2utslipp", "fabPartFilter")),
												array("Mål og vekt", array("lengde", "bredde", "egenvekt", "totalvekt")),
												array("Motor", array("slagvolum", "motorytelse", "antAksler", "antAkslerDrift")),
												array("Dekk/felg", array("stdDekkForan", "stdDekkBak", "stdFelgForan", "stdFelgBak")),
											);

											foreach ( $vognkort_display as $data ) : ?>
												<p><strong><?php echo $data[0]; ?></strong></p>
												<ul>
													<?php 
														foreach ( $data[1] as $value ) {
															echo sprintf( '<li>%s <span class="gridright">%s</span></li>',  $output_data[$value], empty($input_data[$value]) ? '--' : $input_data[$value] );
														}
													?>
												</ul>
											<?php
											endforeach;
										?>
									</div>
								</section>
							</div>
						</div>
					</article>
				</div>
			</section>


		</main>

		<section class="overlay">
			
			<div class="viewport">
				
				<div class="row">
				
					<h1>Bekreft ditt bud</h1>

					<p>Alle bud er juridisk bindende. Bud under reservasjonspris er bindende for budgiver t.o.m 24 timer etter auksjonens sluttid (klokkeslett). Budgiveren får beskjed innen 24 timer dersom budet aksepteres.</p>

					<h2>Ditt bud:</h2>
					<table>
						<colgroup span="3"/>
						<tbody>
							<tr>
								<th>Bil</th>
								<td><span id="bidconfirm-car">Mitsubishi Outlander SUV 4WD</span></td>
							</tr>
							<tr>
								<th>Ditt bud</th>
								<td><span id="bidconfirm-bid">175 000</span>,-</td>
							</tr>
							<tr>
								<th>Autobud</th>
								<td><span id="bidconfirm-autobid">185 000</span>,-</td>
							</tr>
						</tbody>
					</table>

					<form id="verified-bid" method="post" action="">
						<fieldset>
							<input type="hidden" id="verified-bid-car" name="verified-bid-car" value="Mitsubishi Outlander SUV 4WD CAR ID" />
							<input type="hidden" id="verified-bid-bid" name="verified-bid-bid" value="0" />
							<input type="hidden" id="verified-bid-autobid" name="verified-bid-autobid" value="0" />
							<ul class="form-box by-two">
								<li><input type="submit" class="btn green" name="verify-bid" value="Bekreft" /></li>
								<li><a href="#" class="btn red">Avbryt</a></li>
							</ul>
						</fieldset>
					</form>
					
					<a class="close overlay-destroy"><i class="fa fa-times" aria-hidden="true"></i></a>

				</div>
			
			</div>

		</section>
<?php require('footer.php'); ?>
