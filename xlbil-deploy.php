<?php
/**
 * Deploy git repo with webhookses
 *
 * @author Øyvind Eikeland <oyvind@zpirit.no>
 * @version 1.0
 */
// Check if this is a post request from bitbucket webhooks
if( $_SERVER['REQUEST_METHOD'] === "POST" && $_SERVER['HTTP_USER_AGENT'] === "Bitbucket-Webhooks/2.0") {
	// repo variables
	$gitdir = "/var/www/labzpirit/xlbil";
	$gitrepo = "markup-vendo-xlbil";
	$allowed_users = array('oyvindeikeland', 'espenkvalheim');

	// request data from bitbucket
	$data = json_decode(file_get_contents("php://input"));

	// Correct repo
	if( $data->repository->name === $gitrepo ) {
		// If branch is master
		if( $data->push->changes[0]->new->type === "branch" && $data->push->changes[0]->new->name === "master" ) {
			// only deploy if user is allowed
			if ( in_array( $data->actor->username, $allowed_users ) ) {
				$output_cd = shell_exec( 'cd $gitdir');
				$output_git = shell_exec( 'git pull origin master' );
				if( $output_git !== NULL ) {
					echo "Repo updated.";
				}
				else {
					echo "Repo is not updating, check if required php functions are activated.";
				}
			}
			else {
				echo "Only allowed users can deploy this repo.";
			}
		}
		else {
			echo "Only master branch accepted.";
		}
	}
	else {
		echo "Wrong repository.";
	}
}
else {
	die("Logic B00mb!");
}

// When all is said and done.
exit();
?>
